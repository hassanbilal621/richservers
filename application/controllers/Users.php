<?php
class Users extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->library('aws3');
		//$credentials = new Aws\Credentials\Credentials('key', 'secret');
	}
	public function get_my_ip() {

   
        //whether ip is from the share internet  
         if(!empty($_SERVER['HTTP_CLIENT_IP'])) {  
                    $ip = $_SERVER['HTTP_CLIENT_IP'];  
            }  
        //whether ip is from the proxy  
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];  
         }  
    //whether ip is from the remote address  
        else{  
                 $ip = $_SERVER['REMOTE_ADDR'];  
         }  
     
	return $ip;  
	}


	public function logout()
    {

        $this->session->unset_userdata('steamid');

        redirect('users/index');
    }

	public function index(){


		$data['title'] = 'Rich CSGO';

		$databse_servers = $this->admin_model->get_servers();

		if($this->session->userdata('steamid')){

		$json = file_get_contents('http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=79DFEFBCE9FDC3BC28BEB05963008EFB&steamids='.$_SESSION['steamid']);
		$obj = json_decode($json);
		$data['personaname'] = $obj->response->players[0]->personaname;
		$data['avatar'] = $obj->response->players[0]->avatar;
		// $data['realname'] = $obj->response->players[0]->realname;
		$data['profileurl'] = $obj->response->players[0]->profileurl;


		//echo $data['personaname'];
		// $data['avatar'] = $obj->avatar;


		// echo '<pre>';
		// print_r($obj);
		// echo '<pre>';
		// die;


		}


		$data['servers'] = array();


		foreach($databse_servers as $databse_server):

			// $databse_server['server_ip'];
			// $databse_server['server_port'];
			$data['servers'][] = $this->get_serverinfo($databse_server['server_ip'], $databse_server['server_port']);

		endforeach;

		// echo '<pre>';
		// print_r($data['servers']);
		// echo '<pre>';

		// die;
		// $this->load->view('templates/users/header.php');
		$this->load->view('templates/users/index.php', $data);
		// $this->load->view('templates/users/footer.php');
	
	}

	public function get_serverinfo($ip, $port){

		// $ip = "15.185.125.243";
		// $port = "27015";
		
		$socket = socket_create(AF_INET, SOCK_DGRAM, 0);
		$result = socket_connect($socket, $ip, $port);
		
		if($result < 0)
			echo "connect() failed.\nReason: ($result) \n";
		
		$data = "\xFF\xFF\xFF\xFF\x54\x53\x6F\x75\x72\x63\x65\x20\x45\x6E\x67\x69\x6E\x65\x20\x51\x75\x65\x72\x79\x00";
		socket_write($socket, $data, strlen($data));
		
		$out = socket_read($socket, 4096);
	
		
		$queryData         = explode("\x00", substr($out, 6), 5);
		
		$server['ip']		= $ip;
		$server['port']		= $port;
		$server['name']        = $queryData[0];
		$server['map']         = $queryData[1];
		$server['game']        = $queryData[2];
		$server['description'] = $queryData[3];
		$packet                = $queryData[4];
		//$app_id                = array_pop(unpack("S", substr($packet, 0, 2)));
		$server['players']     = ord(substr($packet, 2, 1));
		$server['playersmax']  = ord(substr($packet, 3, 1));
		$server['bots']        = ord(substr($packet, 4, 1));
		$server['dedicated']   =     substr($packet, 5, 1);
		$server['os']          =     substr($packet, 6, 1);
		$server['password']    = ord(substr($packet, 7, 1));
		$server['vac']         = ord(substr($packet, 8, 1));
	
	 return $server;	
	}


	public function logins(){
		$data['title'] = 'Rich CSGO';

		$this->load->view('templates/users/header.php');
		// $this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/login.php', $data);
		$this->load->view('templates/users/footer.php');
	

	}

	public function login_request(){



		include("./assets/openid.php");

		try {
			
			$steamauth['apikey'] = "79DFEFBCE9FDC3BC28BEB05963008EFB"; // Your Steam WebAPI-Key found at https://steamcommunity.com/dev/apikey
			$steamauth['domainname'] = "localhost:81"; // The main URL of your website displayed in the login page
			$steamauth['logoutpage'] = ""; // Page to redirect to after a successfull logout (from the directory the SteamAuth-folder is located in) - NO slash at the beginning!
			$steamauth['loginpage'] = ""; // Page to redirect to after a successfull login (from the directory the SteamAuth-folder is located in) - NO slash at the beginning!
	
			// System stuff
			if (empty($steamauth['apikey'])) {die("<div style='display: block; width: 100%; background-color: red; text-align: center;'>SteamAuth:<br>Please supply an API-Key!<br>Find this in steamauth/SteamConfig.php, Find the '<b>\$steamauth['apikey']</b>' Array. </div>");}
			if (empty($steamauth['domainname'])) {$steamauth['domainname'] = $_SERVER['SERVER_NAME'];}
			// if (empty($steamauth['logoutpage'])) {$steamauth['logoutpage'] = $_SERVER['PHP_SELF'];}
			// if (empty($steamauth['loginpage'])) {$steamauth['loginpage'] = $_SERVER['PHP_SELF'];}

			$openid = new LightOpenID($steamauth['domainname']);
			
			if(!$openid->mode) {
				$openid->identity = 'https://steamcommunity.com/openid';
				header('Location: ' . $openid->authUrl());
			} elseif ($openid->mode == 'cancel') {
				echo 'User has canceled authentication!';
			} else {
				if($openid->validate()) { 
					$id = $openid->identity;
					$ptn = "/^https?:\/\/steamcommunity\.com\/openid\/id\/(7[0-9]{15,25}+)$/";
					preg_match($ptn, $id, $matches);
					
					//$_SESSION['steamid'] = $matches[1];


					$user_data = array(
						'steamid' => $matches[1]
					);
					$this->session->set_userdata($user_data);

					$userip = $this->get_my_ip();

					$this->allow_ip($userip, $matches[1]);

					if (!headers_sent()) {
						//echo $_SESSION['steamid'];
						redirect('users/');
						
					} else {
						redirect('users/');
					}
				} else {
					redirect('users/');
				}
			}
		} catch(ErrorException $e) {
			echo $e->getMessage();
		}



	
	}

	
	public function allow_ip($ip, $steamid)
	{

		date_default_timezone_set('Asia/Karachi');

		include("./vendor/autoload.php");

		$ec2Client = new Aws\Ec2\Ec2Client([
			'region' => 'ap-south-1',
			'version' => '2016-11-15',
			'credentials' => array(
				'key' => 'AKIARNX3M76JJJLJ2FNX',
				'secret'  => '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',
			)
		]);


		$result = $ec2Client->authorizeSecurityGroupIngress(array(


			'GroupId' => 'sg-0b01bcf59345377c8',  //Same GroupName or GroupID as above on line 50
			'IpPermissions' => [
				[
					'FromPort' => '27015',	//Pass multiple values as an array so that all of your policies are defined at once.
					'ToPort' => '27020',
					'IpProtocol' => 'udp',
					'IpRanges' => [
						[
							'CidrIp' => $ip."/32",
							'Description' => $steamid . ' ' . date("m-d-Y H:i:s"),
						],
					],
				],
			],


			
		));
		
		echo '<pre>';
		var_dump($result);
		echo '<pre>';

	}


	public function get_security_groups(){


		include("./vendor/autoload.php");

		$ec2Client = new Aws\Ec2\Ec2Client([
			'region' => 'ap-south-1',
			'version' => '2016-11-15',
			'credentials' => array(
				'key' => 'AKIARNX3M76JJJLJ2FNX',
				'secret'  => '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',
			)
		]);
		
		$result = $ec2Client->describeSecurityGroups();
		
		echo '<pre>';
		print_r($result);
		echo '<pre>';
		 
	}


	// public function login()
	// {
	// 	if ($this->session->userdata('richcsgo_user_id')) {
	// 		redirect('users/');
	// 	}

	// 	$data['title'] = 'Nexco Japan = Login';
	// 	$this->form_validation->set_rules('username', 'Username', 'required');
	// 	$this->form_validation->set_rules('password', 'Password', 'required');

	// 	if ($this->form_validation->run() === FALSE) {

	// 		$this->load->view('templates/users/header.php');
	// 		$this->load->view('templates/users/login.php', $data);
	// 		$this->load->view('templates/users/footer.php');
	// 	} else {
	// 		$username = $this->input->post('username');
	// 		$password = $this->input->post('password');

	// 		$user_id = $this->user_model->login($username, $password);
	// 		//$employee_id = $this->employee_model->login($username, $password);

	// 		if ($user_id) {
	// 			$user_data = array(
	// 				'richcsgo_user_id' => $user_id,
	// 				'username' => $username,
	// 				'necxo_logged_in' => true
	// 			);
	// 			$this->session->set_userdata($user_data);


	// 			//set cookie for 1 year
	// 			$cookie = array(
	// 				'name'   => 'richcsgo_user_id',
	// 				'value'  => $user_id,
	// 				'expire' => time() + 31556926
	// 			);
	// 			$this->input->set_cookie($cookie);


	// 			$this->session->set_flashdata('user_loggedin', 'You are now logged in');
	// 			redirect('users/');
	// 		} else {

	// 			$this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
	// 			redirect('users/login');
	// 		}
	// 	}
	// }


	// public function indexss()
	// {

	// 	include("./vendor/autoload.php");

	// 	$credentials = new Aws\Credentials\Credentials('AKIARNX3M76JJJLJ2FNX', '8uc9YXi9UqTn37JDP2zkAncxMbemcxzdwgoLlaYq');

	// 	$s3 = new Aws\S3\S3Client([
	// 		'version'     => 'latest',
	// 		'region'      => 'us-west-2',
	// 		'credentials' => $credentials
	// 	]);

	// 	// S3 Client 

	// 	$result = $s3->listBuckets();
	// 	echo '<pre>';
	// 	var_dump($result['Buckets']);
	// 	echo '<pre>';
	// 	echo '<br>';




		// $config = array();
		// $config['key'] = 'AKIARNX3M76JJJLJ2FNX';
		// $config['secret'] = '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';
		// $config['region'] = 'us-west-2';
		// $config['version'] = 'latest'; // Or Specified
		// $ec2Client = Aws\Ec2\Ec2Client::factory($config);


		// $result = $ec2Client->DescribeInstances(array(
		// 	'Filters' => array(
		// 			array('Name' => 'instance-type', 'Values' => array('m1.small')),
		// 	)
		// ));


		// $reservations = $result['Reservations'];
		// foreach ($reservations as $reservation) {
		// 	$instances = $reservation['Instances'];
		// 	foreach ($instances as $instance) {

		// 		$instanceName = '';
		// 		foreach ($instance['Tags'] as $tag) {
		// 			if ($tag['Key'] == 'Name') {
		// 				$instanceName = $tag['Value'];
		// 			}
		// 		}


		// 		echo 'Instance Name: ' . $instanceName . PHP_EOL;
		// 		echo '---> State: ' . $instance['State']['Name'] . PHP_EOL;
		// 		echo '---> Instance ID: ' . $instance['InstanceId'] . PHP_EOL;
		// 		echo '---> Image ID: ' . $instance['ImageId'] . PHP_EOL;
		// 		echo '---> Private Dns Name: ' . $instance['PrivateDnsName'] . PHP_EOL;
		// 		echo '---> Instance Type: ' . $instance['InstanceType'] . PHP_EOL;
		// 		echo '---> Security Group: ' . $instance['SecurityGroups'][0]['GroupName'] . PHP_EOL;
		// 	}

		// }


		//new Aws\Credentials\Credentials('AKIARNX3M76JJJLJ2FNX', '8uc9YXi9UqTn37JDP2zkAncxMbemcxzdwgoLlaYq');
		// $ec2Client = new Aws\Ec2\Ec2Client([
		// 	'region' => 'us-west-2',
		// 	'version' => 'latest',
		// 	'credentials' => array(
		// 		'key' => 'AKIARNX3M76JJJLJ2FNX',
		// 		'secret'  => '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',
		// 	)
		// ]);

		// $result = $ec2Client->describeInstances();
		// echo '<pre>';
		// var_dump($result);
		// echo '<pre>';
		// EC2 Client
		// $ec2 = $sdk->createEC2();
		// $reservations = $ec2->describeInstances();

		// Get instance information
		// foreach ($reservations as $r)
		// {
		// 	 $instances = $r['Instances'];

		// Iterate through instances
		//  foreach ($instances as $i)
		// 	 {
		// 		var_dump($i);
		// 	 }
		// }

	// }


	public function stopinstane()
	{

		include("./vendor/autoload.php");


		// snippet-end:[ec2.php.start_and_stop_instance.import]
		/**
		 * Start and Stop Instances
		 *
		 * This code expects that you have AWS credentials set up per:
		 * https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials.html
		 */


		// snippet-start:[ec2.php.start_and_stop_instance.main]
		$ec2Client = new Aws\Ec2\Ec2Client([
			'region' => 'ap-south-1',
			'version' => '2016-11-15',
			'credentials' => array(
				'key' => 'AKIARNX3M76JJJLJ2FNX',
				'secret'  => '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',
			)
		]);

		$action = 'STOP';

		$instanceIds = array('i-0b82de45b2df69f2c');

		if ($action == 'START') {
			$result = $ec2Client->startInstances(array(
				'InstanceIds' => $instanceIds,
			));
		} else {
			$result = $ec2Client->stopInstances(array(
				'InstanceIds' => $instanceIds,
			));
		}

		echo '<pre>';
		var_dump($result);
		echo '<pre>';
	}



	public function startinstane()
	{

		include("./vendor/autoload.php");


		// snippet-end:[ec2.php.start_and_stop_instance.import]
		/**
		 * Start and Stop Instances
		 *
		 * This code expects that you have AWS credentials set up per:
		 * https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials.html
		 */


		// snippet-start:[ec2.php.start_and_stop_instance.main]
		$ec2Client = new Aws\Ec2\Ec2Client([
			'region' => 'ap-south-1',
			'version' => '2016-11-15',
			'credentials' => array(
				'key' => 'AKIARNX3M76JJJLJ2FNX',
				'secret'  => '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',
			)
		]);

		$action = 'START';

		$instanceIds = array('i-0b82de45b2df69f2c');

		if ($action == 'START') {
			$result = $ec2Client->startInstances(array(
				'InstanceIds' => $instanceIds,
			));
		} else {
			$result = $ec2Client->stopInstances(array(
				'InstanceIds' => $instanceIds,
			));
		}

		echo '<pre>';
		var_dump($result);
		echo '<pre>';
	}

	// public function allow_ipss()
	// {

	// 	date_default_timezone_set('America/Los_Angeles');

	// 	include("./vendor/autoload.php");

	// 	//DynDNS Hosts
	// 	$dynDNS['name1'] = "host1.ddns.net";
	// 	$dynDNS['name2'] = "host2.ddns.net";
	// 	$dynDNS['name3'] = "host3.ddns.net";


	// 	//Clients and Regions
	// 	$client['client1']['profile'] = "default";	//Corresponds to the credentials file
	// 	$client['client1']['regions'] = array('us-west-1', 'us-east-2');

	// 	$client['client2']['profile'] = "XXXXXXXX";	//If you don't need to manage multiple AWS accounts, you can remove this index
	// 	$client['client2']['regions'] = array('us-west-1', 'us-west-2');

	// 	//Loop through the array
	// 	foreach ($client as $key => $val) {

	// 		//Set Profile
	// 		$profile = $val['profile'];

	// 		//Loop through the regions
	// 		foreach ($val['regions'] as $k => $v) {

	// 			$ec2Client = new Aws\Ec2\Ec2Client([
	// 				'region' => $v,
	// 				'version' => '2016-11-15',
	// 				'profile' => $profile
	// 			]);

	// 			//Describe the Policy
	// 			$result = $ec2Client->describeSecurityGroups(array(
	// 				'GroupNames' => ['XXXXXXXX'],	//The name of the group you want to manage, you can also sub this out for the GroupID by changing the key value.
	// 			));

	// 			//Get all of the IP's attached to the policy
	// 			foreach ($result['SecurityGroups']['0']['IpPermissions'] as $policy) {
	// 				foreach ($policy['IpRanges'] as $listedIP) {
	// 					$ipRange[] = explode("/", $listedIP['CidrIp'])['0'];
	// 				}
	// 			}

	// 			//Sanatize the IP array
	// 			$ipListClean = array_unique($ipRange);

	// 			//Compare the IP addresses against the policy
	// 			foreach ($dynDNS as $clientHostName) {
	// 				$resolvedIP = gethostbyname($clientHostName);
	// 				if (!in_array($resolvedIP, $ipListClean)) {

	// 					//Add this IP to the Policy
	// 					$result = $ec2Client->authorizeSecurityGroupIngress(array(
	// 						'GroupName' => 'XXXXXXXX',  //Same GroupName or GroupID as above on line 50
	// 						'IpPermissions' => [
	// 							[
	// 								'FromPort' => '22',  //You can configuire the fromPort and ToPorts to declare a range, this example allows up SSH.
	// 								'ToPort' => '22',
	// 								'IpProtocol' => 'tcp',
	// 								'IpRanges' => [
	// 									[
	// 										'CidrIp' => $resolvedIP . '/32',
	// 										'Description' => $clientHostName . ' ' . date("m-d-Y H:i:s"),
	// 									],
	// 								],
	// 							],
	// 							[
	// 								'FromPort' => '10000',	//Pass multiple values as an array so that all of your policies are defined at once.
	// 								'ToPort' => '10100',
	// 								'IpProtocol' => 'tcp',
	// 								'IpRanges' => [
	// 									[
	// 										'CidrIp' => $resolvedIP . '/32',
	// 										'Description' => $clientHostName . ' ' . date("m-d-Y H:i:s"),
	// 									],
	// 								],
	// 							],
	// 						],
	// 					));

	// 					if ($profile == "client2") {  //Want to make an additional change to only a specific group, do it here.   If not, you can omit this section entirely.
	// 						$result = $ec2Client->authorizeSecurityGroupIngress(array(
	// 							'GroupName' => 'XXXXXXXXXXXXX',
	// 							'IpPermissions' => [
	// 								[
	// 									'FromPort' => '80',
	// 									'ToPort' => '80',
	// 									'IpProtocol' => 'tcp',
	// 									'IpRanges' => [
	// 										[
	// 											'CidrIp' => $resolvedIP . '/32',
	// 											'Description' => $clientHostName . ' ' . date("m-d-Y H:i:s"),
	// 										],
	// 									],
	// 								],
	// 								[
	// 									'FromPort' => '443',
	// 									'ToPort' => '443',
	// 									'IpProtocol' => 'tcp',
	// 									'IpRanges' => [
	// 										[
	// 											'CidrIp' => $resolvedIP . '/32',
	// 											'Description' => $clientHostName . ' ' . date("m-d-Y H:i:s"),
	// 										],
	// 									],
	// 								],
	// 							],
	// 						));
	// 					}
	// 				}
	// 			}

	// 			unset($ipRange);
	// 		}
	// 	}
	// }


}
