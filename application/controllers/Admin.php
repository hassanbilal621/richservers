<?php
class admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
    }

    public function login()
    {

        if ($this->session->admindata('azmi_admin_id')) {
            redirect('admin/');
        }

        $data['title'] = 'Nexco Japan';

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === FALSE) {

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/login.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {

            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $azmi_admin_id = $this->admin_model->login($username, $password);

            if ($azmi_admin_id) {
                $admin_data = array(
                    'azmi_admin_id' => $azmi_admin_id,
                    'japan_email' => $username,
                    'submit_alogged_in' => true
                );
                $this->session->set_admindata($admin_data);

                redirect('admin/');
            } else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('admin/login');
            }
        }
    }

    public function logout()
    {

        $this->session->unset_userdata('azmi_admin_id');
        $this->session->unset_userdata('japan_email');
        $this->session->unset_userdata('submit_alogged_in');

        $this->session->set_flashdata('admin_loggedout', 'You are now logged out');
        redirect('admin/login');
    }

    function do_upload()
    {

        $config = array(
            'upload_path' => "assets/uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => false,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "5000",
            'max_width' => "5000"
        );

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {
            $imgdata = array('upload_data' => $this->upload->data());

            $imgname = $imgdata['upload_data']['file_name'];
        } else {
            $error = array('error' => $this->upload->display_errors());
            echo '<pre>';
            print_r($error);
            echo '<pre>';
            exit;
        }

        return $imgname;
    }

    public function index()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $data['suppliers'] = $this->admin_model->get_suppliers();
        $data['companies'] = $this->admin_model->get_companies();
        $data['customers'] = $this->admin_model->get_customer();
        $data['total_product'] = $this->admin_model->total_product();
        // $data['total_supplier_product'] = $this->admin_model->total_supplier_product($suppllierid);
        $data['total_supplier'] = $this->admin_model->total_supplier();
        $data['total_purchase_order'] = $this->admin_model->total_purchase_order();
        $data['total_invoicess'] = $this->admin_model->total_invoices();
        // $data['total_company_invoices'] = $this->admin_model->total_company_invoices($companyid);
        $data['total_stock'] = $this->admin_model->total_stock();
        $data['total_customer'] = $this->admin_model->total_customer();




        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/dashboard.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    // Product Order Manager Start

    public function addproduct()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }
        $this->form_validation->set_rules('product', 'product', 'required');


        if ($this->form_validation->run() === FALSE) {

            $data['categories'] = $this->admin_model->get_cat();
            $data['suppliers'] = $this->admin_model->get_suppliers();
            $data['companies'] = $this->admin_model->get_companies();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/addproduct.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $imgname = $this->do_upload();

            $this->admin_model->add_product($imgname);
            redirect('admin/addproduct');
        }
    }

    public function manageproduct()
    {


        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('product', 'product', 'required');

        if ($this->form_validation->run() === FALSE) {

            $data['products'] = $this->admin_model->get_products();
            $data['categories'] = $this->admin_model->get_cat();
            $data['suppliers'] = $this->admin_model->get_suppliers();
            $data['companies'] = $this->admin_model->get_companies();


            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/manageproduct.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            if (!file_exists($_FILES["userfile"]["tmp_name"])) {
                $productid = $this->input->post('productid');
                $currUser = $this->admin_model->get_ajax_product($productid);

                $imgname = $currUser["pro_img"];

                $this->admin_model->updateproduct($imgname,  $productid);
            } else {
                $imgname = $this->do_upload();
                $productid = $this->input->post('productid');
                $this->admin_model->updateproduct($imgname,  $productid);
            }

            redirect('admin/manageproduct');
        }
    }

    public function deleteproduct($productid)
    {

        $this->admin_model->deleteproduct($productid);
        redirect('admin/manageproduct');
    }

    // Product Order Manager End

    // Suppliers Order Manager Start


    public function addsuppliers()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('suppliers', 'suppliers', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');


        if ($this->form_validation->run() === FALSE) {
            $data['title'] = "Add Suppliers";

            $data['suppliers'] = $this->admin_model->get_suppliers();
            $data['companies'] = $this->admin_model->get_companies();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/addsuppliers.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $imgname = $this->do_upload();

            $this->admin_model->addsuppliers($imgname);

            redirect('admin/addsuppliers');
        }
    }

    public function managesuppliers()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('suppliers', 'suppliers', 'required');

        if ($this->form_validation->run() === FALSE) {


            $data['suppliers'] = $this->admin_model->get_suppliers();
            $data['companies'] = $this->admin_model->get_companies();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/managesuppliers.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            if (!file_exists($_FILES["userfile"]["tmp_name"])) {
                $supplierid = $this->input->post('supplierid');
                $currUser = $this->admin_model->get_supplier($supplierid);

                $imgname = $currUser["pro_img"];

                $this->admin_model->updatesupplier($imgname,  $supplierid);
            } else {
                $imgname = $this->do_upload();
                $supplierid = $this->input->post('supplierid');
                $this->admin_model->updatesupplier($imgname,  $supplierid);
            }


            redirect('admin/managesuppliers');
        }
    }

    public function suppliersdelete($supplierid)
    {

        $this->admin_model->del_suppliers($supplierid);
        redirect('admin/managesuppliers');
    }

    // Suppliers Order Manager End




    // purchase Order Manager Start

    public function addpurchaseorder()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }


        $this->form_validation->set_rules('supplier', 'Supplier', 'required');



        if ($this->form_validation->run() === FALSE) {


            $data['suppliers'] = $this->admin_model->get_suppliers();

            $data['products'] = $this->admin_model->get_products();
            $data['companies'] = $this->admin_model->get_companies();




            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/addpurchaserorder.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {


            $orderid = $this->admin_model->add_purchase_order();


            $number = count($_POST["products"]);
            if ($number > 1) {
                for ($i = 0; $i < $number; $i++) {
                    if (trim($_POST["products"][$i] != '')) {
                        $this->addpurchaseorderitem($orderid, $_POST["products"][$i], $_POST["qty"][$i], $_POST["discount"][$i], $_POST["tradeprice"][$i], $_POST["total"][$i]);
                    }
                }
                echo "Data Inserted";
            }


            // die;
            redirect('admin/addpurchaseorder?success=1');
        }
    }

    public function addpurchaseorderitem($orderid, $productid, $qty, $discount, $tradeprice, $total)
    {

        $data = array(
            'purchase_order_id' => $orderid,
            'product_name_id' => $productid,
            'Qty' => $qty,
            'discount' => $discount,
            'trade_price' => $tradeprice,
            'netamount' => $total
        );

        $this->security->xss_clean($data);
        $this->db->insert('purchase_order_item', $data);
    }

    public function managepurchaseorder()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('supplier_name_id', 'supplier_name_id', 'required');



        if ($this->form_validation->run() === FALSE) {

            $data['purchaseorders'] = $this->admin_model->get_purchase_order();
            $data['companies'] = $this->admin_model->get_companies();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/managepurchaceorder.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->get_purchase_order();

            redirect('admin/managepurchaseorder');
        }
    }

    public function viewpurchaseorder($purchaseorderid)
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $data['purchaseorder'] = $this->admin_model->get_purchaseorder($purchaseorderid);
        $data['purchaseorderitems'] = $this->admin_model->get_purchaseorderitems($purchaseorderid);
        $data['companies'] = $this->admin_model->get_companies();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/viewpurchaseorder.php', $data);
        $this->load->view('templates/admin/footer.php');
    }


    public function pandingpurchaseorder()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $data['purchaseorders'] = $this->admin_model->get_pending_order();
        $data['companies'] = $this->admin_model->get_companies();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/pendingorder.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function update_pending_order($purchaseorderid)
    {

        $this->admin_model->update_pending_order($purchaseorderid);
        redirect('admin/managepurchaseorder');
    }

    public function recivedpurchaseorder()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('order_name', 'order_name', 'required');


        $data['purchaseorders'] = $this->admin_model->get_recived_order();
        $data['companies'] = $this->admin_model->get_companies();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/recivedpurchaseorder.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function update_recived_order($purchaseorderid)
    {

        $this->admin_model->update_recived_order($purchaseorderid);
        redirect('admin/managepurchaseorder');
    }

    public function cancelpurchaseorder()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('order_name', 'order_name', 'required');


        $data['purchaseorders'] = $this->admin_model->get_cancel_order();
        $data['companies'] = $this->admin_model->get_companies();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/cancelpurchaseorder.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function update_cancel_order($purchaseorderid)
    {

        $this->admin_model->update_cancel_order($purchaseorderid);
        redirect('admin/managepurchaseorder');
    }

    // purchase Order Manager End

    // invoice / add / manage/ delete Start

    public function addinvoice()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }


        $this->form_validation->set_rules('customer_id', 'customer_id', 'required');
        $this->form_validation->set_rules('companyid', 'companyid', 'required');
        $this->form_validation->set_rules('product_id', 'product_id', 'required');



        if ($this->form_validation->run() === FALSE) {


            $data['customers'] = $this->admin_model->get_customer();
            $data['companies'] = $this->admin_model->get_companies();
            $data['products'] = $this->admin_model->get_products();
            $data['stocks'] = $this->admin_model->get_stocks();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/addinvoice.php');
            $this->load->view('templates/admin/footer.php');
        } else {


            $orderid = $this->admin_model->addinvoice();


            $number = count($_POST["products"]);
            if ($number > 1) {
                for ($i = 0; $i < $number; $i++) {
                    if (trim($_POST["products"][$i] != '')) {
                        $this->addinvoiceitem($orderid, $_POST["products"][$i], $_POST["qty"][$i], $_POST["stockid"][$i], $_POST["discount"][$i], $_POST["marketprice"][$i], $_POST["total"][$i]);
                        $this->adddeliveritem($orderid, $_POST["products"][$i], $_POST["qty"][$i], $_POST["stockid"][$i]);
                    }
                }
                echo "Data Inserted";
            }

            redirect('admin/addinvoice?success=1');
        }
    }

    public function get_stock_by_poductid($productid)
    {
        $data['stocks'] = $this->admin_model->get_stock_by_poductid($productid);
        $this->load->view('templates/ajax/getstock.php', $data);
    }
    public function adddeliveritem($orderid, $productid, $qty, $stockid)
    {
        $stockdata =  $this->admin_model->get_ajax_stock($stockid);
        if ($stockdata['stock_product_id'] =   $productid) {
            $last_used_stock = $stockdata['used_stock'];
            $used_stock = $last_used_stock + $qty;

            $this->admin_model->update_usedstock($used_stock, $stockid);

            $data = array(
                'invoiceid' => $orderid,
                'productid' => $productid,
                'Stockqty' => $qty,
                'Stock_id' => $stockid,

            );

            $this->security->xss_clean($data);
            $this->db->insert('deliverstock', $data);
        } else { }
    }
    public function addinvoiceitem($orderid, $productid, $qty, $stockid, $discount, $marketprice, $total)
    {

        $data = array(
            'invoice_id' => $orderid,
            'product_name_id' => $productid,
            'Qty' => $qty,
            'discount' => $discount,
            'stockid' => $stockid,
            'market_price' => $marketprice,
            'netamount' => $total
        );

        $this->security->xss_clean($data);
        $this->db->insert('invoice_item', $data);
    }



    public function manageinvoice()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('invoice', 'invoice', 'required');

        if ($this->form_validation->run() === FALSE) {


            $data['invoices'] = $this->admin_model->get_invoices();
            $data['companies'] = $this->admin_model->get_companies();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/manageinvoice.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->invoice();

            redirect('admin/manageinvoice');
        }
    }

    public function viewinvoice($invoiceid)
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $data['invoice'] = $this->admin_model->get_invoice($invoiceid);
        $data['invoiceitems'] = $this->admin_model->get_invoiceItem($invoiceid);
        $data['companies'] = $this->admin_model->get_companies();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/viewinvoice.php', $data);
        $this->load->view('templates/admin/footer.php');
    }
    // Invoice / Add / Manage/ Delete End

    // Company Order Manager Start


    public function addcompany()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('company', 'company', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');


        if ($this->form_validation->run() === FALSE) {
            $data['companies'] = $this->admin_model->get_companies();
            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/addcompany.php');
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->addcompany();

            redirect('admin/addcompany');
        }
    }

    public function managecompany()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('companyname', 'companyname', 'required');

        if ($this->form_validation->run() === FALSE) {


            $data['companies'] = $this->admin_model->get_companies();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/managecompany.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->updatecompany();

            redirect('admin/managecompany');
        }
    }

    public function company($companyid)
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }


        $data['companies'] = $this->admin_model->get_companies();
        $data['company'] = $this->admin_model->get_company($companyid);
        $data['invoices'] = $this->admin_model->get_invoice($companyid);

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/company.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    // company Order Manager End

    // customer Order Manager Start

    public function addcustomer()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('customer_name', 'customer_name', 'required');
        $this->form_validation->set_rules('billing_address', 'billing_address', 'required');


        if ($this->form_validation->run() === FALSE) {

            $data['companies'] = $this->admin_model->get_companies();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/addcustomer.php');
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->addcustomer();

            redirect('admin/addcustomer');
        }
    }
    public function managecustomer()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('customer_name', 'customer_name', 'required');

        if ($this->form_validation->run() === FALSE) {


            $data['customers'] = $this->admin_model->get_customer();
            $data['companies'] = $this->admin_model->get_companies();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/managecustomer.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->updatecustomer();

            redirect('admin/managecustomer');
        }
    }

    public function customerdelete($customerid)
    {

        $this->admin_model->del_customer($customerid);
        redirect('admin/managecustomer');
    }

    // customer Order Manager End



    // stock / add / manage/ delete Start

    public function addstock()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }
        $this->form_validation->set_rules('new_stock', 'new_stock', 'required');

        if ($this->form_validation->run() === FALSE) {
            $data['companies'] = $this->admin_model->get_companies();
            $data['products'] = $this->admin_model->get_products();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/addstock.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $productid =  $this->input->post('stock_product_id');

            $this->admin_model->addstock($productid);


            redirect('admin/addstock');
        }
    }
    public function managestock()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('new_stock', 'new_stock', 'required');

        if ($this->form_validation->run() === FALSE) {


            $data['stocks'] = $this->admin_model->get_stocks();
            $data['companies'] = $this->admin_model->get_companies();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/managestock.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {


            $stockid =  $this->input->post('stock_id');

            $this->admin_model->updatestock($stockid);


            redirect('admin/managestock');
        }
    }

    public function stockdelete($stockid)
    {

        $this->admin_model->del_stock($stockid);
        redirect('admin/managestock');
    }

    public function deliversock()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }
        $data['products'] = $this->admin_model->get_products();
        $data['stocks'] = $this->admin_model->get_stocks();
        $data['companies'] = $this->admin_model->get_companies();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/deliversock.php', $data);
        $this->load->view('templates/admin/footer.php');
    }


    // payment / Add / Manage/ Delete End

    public function payment()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }
        $this->form_validation->set_rules('payment', 'payment', 'required');

        $purchaseorderid = $this->input->post('purchaseorderid');

        if ($this->form_validation->run() === FALSE) {
            redirect('admin/viewpurchaseorder/' . $purchaseorderid);
        } else {
            $purchaseorderdata = $this->admin_model->get_purchaseorder($purchaseorderid);
            $paid_amount = $purchaseorderdata['paid_amount'];
            $payment = $this->input->post('payment');
            $total_paid = $paid_amount + $payment;
            $this->admin_model->update_purchaseorder($total_paid, $purchaseorderid);
            $this->admin_model->payment($purchaseorderid);
            redirect('admin/viewpurchaseorder/' . $purchaseorderid);
        }
    }

    public function managepayment()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('payment', 'payment', 'required');

        if ($this->form_validation->run() === FALSE) {


            $data['payments'] = $this->admin_model->get_payments();
            $data['companies'] = $this->admin_model->get_companies();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/managepayment.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $paymentid = $this->input->post('payment_id');
            $paymentdata = $this->admin_model->get_payment($paymentid);
            $lastpayment = $paymentdata['payment'];

            $purchaseorderid = $this->input->post('purchass_order_id_payment');
            $purchaseorderdata = $this->admin_model->get_purchaseorder($purchaseorderid);
            $paid_amount = $purchaseorderdata['paid_amount'];



            $total_paid = $paid_amount - $lastpayment;


            $payment = $this->input->post('payment');

            $nettotal_paid = $total_paid + $payment;
            $this->admin_model->update_purchaseorder($nettotal_paid, $purchaseorderid);
            $this->admin_model->update_payment($paymentid);
            redirect('admin/managepayment');
        }
    }

    public function paymentdelete($paymentid, $purchaseorderid)
    {
        $paymentdata = $this->admin_model->get_payment($paymentid);
        $lastpayment = $paymentdata['payment'];
        $purchaseorderdata = $this->admin_model->get_purchaseorder($purchaseorderid);
        $paid_amount = $purchaseorderdata['paid_amount'];
        $total_paid = $paid_amount - $lastpayment;

        $this->admin_model->update_purchaseorder($total_paid, $purchaseorderid);
        $this->admin_model->del_payment($paymentid);
        redirect('admin/managepayment');
    }

    // payment / Add / Manage/ Delete End

    // report / add / manage/ delete Start

    public function salereport()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }
        $data['companies'] = $this->admin_model->get_companies();
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/salereport.php');
        $this->load->view('templates/admin/footer.php');
    }
    public function get_sale_report()
    {
        $data['invoicess'] = $this->admin_model->get_sale_report();

        $this->load->view('templates/ajax/salereport.php', $data);
    }

    public function purchasereport()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $data['payments'] = $this->admin_model->get_payments();
        $data['companies'] = $this->admin_model->get_companies();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/purchasereport.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function get_Purchase_report()
    {
        $data['purchaseorders'] = $this->admin_model->get_Purchase_report();

        $this->load->view('templates/ajax/purchasereport.php', $data);
    }


    public function expencereport()
    {

        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('payment', 'payment', 'required');

        if ($this->form_validation->run() === FALSE) {


            $data['payments'] = $this->admin_model->get_payments();
            $data['companies'] = $this->admin_model->get_companies();
            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/expencereport.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->payment();

            redirect('admin/managepayment');
        }
    }
    public function get_expence_report()
    {
        $data['expencess'] = $this->admin_model->get_expence_report();

        $this->load->view('templates/ajax/expencereport.php', $data);
    }

    // report / Add / Manage/ Delete End

    // expence / add / manage/ delete Start

    public function addexpencecatagoty()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }
        $this->form_validation->set_rules('exp_cat', 'exp_cat', 'required');

        if ($this->form_validation->run() === FALSE) {

            $data['categories'] = $this->admin_model->get_expence_cat();
            $data['companies'] = $this->admin_model->get_companies();
            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/addexpencecatagoty.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->addexpencecatagoty();

            redirect('admin/addexpencecatagoty');
        }
    }
    public function addexpence()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('expence_price', 'expence_price', 'required');

        if ($this->form_validation->run() === FALSE) {

            $data['categories'] = $this->admin_model->get_expence_act_cat();
            $data['companies'] = $this->admin_model->get_companies();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/addexpence.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->addexpence();

            redirect('admin/addexpence');
        }
    }

    public function update_exp_cat()
    {
        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('exp_cat', 'exp_cat', 'required');

        if ($this->form_validation->run() === FALSE) { } else {
            $this->admin_model->update_exp_cat();

            redirect('admin/addexpencecatagoty');
        }
    }


    public function manageexpence()
    {

        if (!$this->session->admindata('azmi_admin_id')) {
            redirect('admin/login');
        }
        $this->form_validation->set_rules('exp_cat', 'exp_cat', 'required');

        if ($this->form_validation->run() === FALSE) {

            $data['expencess'] = $this->admin_model->get_expence();
            $data['companies'] = $this->admin_model->get_companies();
            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/manageexpence.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->updateexpence();

            redirect('admin/manageexpence');
        }
    }

    public function cat_delete($expenceid)
    {

        $this->admin_model->cat_delete($expenceid);
        redirect('admin/addexpencecatagoty');
    }
    public function expence_delete($expenceid)
    {

        $this->admin_model->expence_delete($expenceid);
        redirect('admin/manageexpence');
    }

    public function activestatus($expenceid)
    {

        $this->admin_model->activestatus($expenceid);
        redirect('admin/addexpencecatagoty');
    }
    public function deactivestatus($expenceid)
    {

        $this->admin_model->deactivestatus($expenceid);
        redirect('admin/addexpencecatagoty');
    }


    // expence / Add / Manage/ Delete End

    ////***************************ajax********************************************////////////////
    public function ajax_edit_payment($paymentid)
    {
        $data['payment'] =  $this->admin_model->get_ajax_payment($paymentid);

        $this->load->view('templates/ajax/editpayment.php', $data);
    }

    public function ajax_edit_stock($stockid)
    {
        $data['products'] = $this->admin_model->get_products();
        $data['stock'] =  $this->admin_model->get_ajax_stock($stockid);

        $this->load->view('templates/ajax/editstock.php', $data);
    }

    public function ajax_edit_order_adminmodal($orderid)
    {

        $data['order'] =  $this->admin_model->get_ajax_stock($orderid);

        $this->load->view('templates/ajax/editorder.php', $data);
    }

    public function ajax_edit_supplier_adminmodal($supplierid)
    {

        $data['supplier'] =  $this->admin_model->get_ajax_supplier($supplierid);

        $this->load->view('templates/ajax/editsupplier.php', $data);
    }

    public function get_products_ajax_details($productid)
    {
        $product_details = $this->admin_model->get_products_ajax_details($productid);
        echo '<pre>';
        print_r($product_details);
        echo '<pre>';
    }

    public function ajax_edit_product_adminmodal($productid)
    {

        $data['product'] =  $this->admin_model->get_ajax_product($productid);
        $data['categories'] = $this->admin_model->get_cat();
        $data['suppliers'] = $this->admin_model->get_suppliers();



        $this->load->view('templates/ajax/editproduct.php', $data);
    }

    public function ajax_edit_purchase_order_adminmodal($purchaseorderid)
    {

        $data['product'] =  $this->admin_model->get_ajax_purchase_order($purchaseorderid);
        $data['suppliers'] = $this->admin_model->get_suppliers();



        $this->load->view('templates/ajax/editpurchseorder.php', $data);
    }

    public function ajax_get_products_details($productid)
    {

        $myObj =  $this->admin_model->get_ajax_product($productid);

        $myJSON = json_encode($myObj);

        echo $myJSON;
    }

    public function ajax_edit_company_adminmodal($companyid)
    {

        $data['company'] = $this->admin_model->get_company($companyid);


        $this->load->view('templates/ajax/editcompany.php', $data);
    }

    public function ajax_Invoice($invoiceid)
    {

        $data['invoice'] = $this->admin_model->get_invoice($invoiceid);


        $this->load->view('templates/ajax/editinvoice.php', $data);
    }

    public function ajax_edit_catagory_adminmodal($expenceid)
    {

        $data['catagory'] = $this->admin_model->get_catagory_ajax($expenceid);


        $this->load->view('templates/ajax/editexpencecatagoty.php', $data);
    }

    public function ajax_edit_expence_adminmodal($expenceid)
    {

        $data['expence'] = $this->admin_model->get_expence_ajax($expenceid);

        $data['categories'] = $this->admin_model->get_expence_cat();


        $this->load->view('templates/ajax/editexpence.php', $data);
    }

    public function ajax_get_product_details($productid)
    {
        $myObj =  $this->admin_model->get_ajax_product($productid);
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }
    public function ajax_get_stock_details($stockid)
    {
        $myObj =  $this->admin_model->get_ajax_stock($stockid);
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }

    public function ajax_edit_customer_adminmodal($customerid)
    {

        $data['customer'] = $this->admin_model->get_customer_ajax($customerid);


        $this->load->view('templates/ajax/editcustomer.php', $data);
    }

    public function ajax_view_profile($supplierid)
    {
        $data['total_supplier_product'] = $this->admin_model->total_supplier_product($supplierid);
        $data['supplier'] =  $this->admin_model->get_ajax_supplier($supplierid);

        $this->load->view('templates/ajax/profile.php', $data);
    }

    public function deleterowcart($rowid)
    {
        $this->cart->remove($rowid);
        redirect('users/cart');
    }
}
