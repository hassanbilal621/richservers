<head>
    <meta charset="UTF-8">
    <title>Rich Servers</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/richcsgo/font/iconsmind-s/css/iconsminds.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/richcsgo/font/simple-line-icons/css/simple-line-icons.css" />

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/richcsgo/css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/richcsgo/css/vendor/bootstrap.rtl.only.min.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/richcsgo/css/vendor/component-custom-switch.min.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/richcsgo/css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>css/vendor/perfect-scrollbar.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/richcsgo/css/dore.dark.blue.css" />




</head>