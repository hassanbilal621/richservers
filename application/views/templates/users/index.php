<head>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/richcsgo/font/iconsmind-s/css/iconsminds.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/richcsgo/font/simple-line-icons/css/simple-line-icons.css" />

<link rel="stylesheet" href="<?php echo base_url() ?>assets/richcsgo/css/vendor/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/richcsgo/css/vendor/bootstrap.rtl.only.min.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/richcsgo/css/main.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/richcsgo/css/dore.dark.blue.css" />
</head>

<body id="app-container" class="menu-default menu-sub-hidden">
    <nav class="navbar fixed-top">


        <a class="navbar-logo">
            <span class="d-none d-xs-block"><h1><b>  <img src="https://www.freeiconspng.com/uploads/steam-logo-png-8.png" height="24px" alt="uploaded image" class="list-media-thumbnail responsive border-0"> Rich Servers</b></h1></span>
            <span class="logo-mobile d-block d-xs-none"></span>
        </a>

        <div class="navbar-right">
            <div class="header-icons d-inline-block align-middle">
                    <?php if(!$this->session->userdata('steamid')){ ?> <a href="<?php echo base_url()?>users/login_request" type="button" class="btn btn-outline-secondary mb-1"> <img src="https://www.freeiconspng.com/uploads/steam-logo-png-8.png" height="24px" alt="uploaded image" class="list-media-thumbnail responsive border-0"> Sign in with Steam to play on Listed Server</a> <?php }else{  ?>  <a href="<?php echo base_url()?>users/logout" type="button" class="btn btn-outline-info default mb-1"> <img src="<?php echo $avatar ?>" height="24px" alt="uploaded image" class="list-media-thumbnail responsive border-0">  &nbsp  &nbsp Logout</a>  <?php } ?>
                </div>


            </div>

         
        </div>
    </nav>

    <div class="menu">
        <div class="main-menu">
            <div class="scroll">
                <ul class="list-unstyled">
                    <li>
                        <a class="active">
                            <i class="iconsminds-shop-4"></i>
                            <span>Servers</span>
                        </a>
                    </li>
                    <li>
                        <a href="#layouts">
                            <i class="iconsminds-digital-drawing"></i> Sign in Admin
                        </a>
                    </li>
              
              
                </ul>
            </div>
        </div>

    </div>

    <main>
        <div class="container-fluid disable-text-selection">


        <div class="row">
                <div class="col-lg-4">
                    <div class="card mb-4 progress-banner">
                        <div class="card-body justify-content-between d-flex flex-row align-items-center">
                            <div>
                                <i class="iconsminds-gamepad-2 mr-2 text-white align-text-bottom d-inline-block"></i>
                                <div>
                                    <p class="lead text-white">2 Servers</p>
                                    
                                </div>
                            </div>

                            <div>
                                <div role="progressbar"
                                    class="progress-bar-circle progress-bar-banner position-relative" data-color="white"
                                    data-trail-color="rgba(255,255,255,0.2)" aria-valuenow="5" aria-valuemax="12"
                                    data-show-percent="false">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card mb-4 progress-banner">
                        <div class="card-body justify-content-between d-flex flex-row align-items-center">
                            <div>
                                <i class="iconsminds-male mr-2 text-white align-text-bottom d-inline-block"></i>
                                <div>
                                    <p class="lead text-white">4 Users</p>
                                    
                                </div>
                            </div>
                            <div>
                                <div role="progressbar"
                                    class="progress-bar-circle progress-bar-banner position-relative" data-color="white"
                                    data-trail-color="rgba(255,255,255,0.2)" aria-valuenow="4" aria-valuemax="6"
                                    data-show-percent="false">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card mb-4 progress-banner">
                        <a href="#" class="card-body justify-content-between d-flex flex-row align-items-center">
                            <div>
                                <i class="iconsminds-bell mr-2 text-white align-text-bottom d-inline-block"></i>
                                <div>
                                    <p class="lead text-white">0 Blocked Users</p>
                                 
                                </div>
                            </div>
                            <div>
                                <div role="progressbar"
                                    class="progress-bar-circle progress-bar-banner position-relative" data-color="white"
                                    data-trail-color="rgba(255,255,255,0.2)" aria-valuenow="8" aria-valuemax="10"
                                    data-show-percent="false">
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12">
        
                    <div class="separator mb-5"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 list" data-check-all="checkAll">


                    <?php foreach($servers as $server): ?>

                    <div class="card d-flex flex-row mb-3">
                        <a class="d-flex" href="Pages.Product.Detail.html">
                            <img src="https://i.imgur.com/uHP07Bj.png" alt="Fat Rascal"
                                class="list-thumbnail responsive border-0 card-img-left" />
                        </a>
                        <div class="pl-2 d-flex flex-grow-1 min-width-zero">
                            <div
                                class="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
                                <a href="" class="w-40 w-sm-100">
                                    <p class="list-item-heading mb-0 truncate"><?php echo $server['name']; ?></p>
                                </a>
                                <p class="mb-0 text-muted text-heading w-15 w-sm-100"><?php echo $server['ip']; ?>:<?php echo $server['port']; ?></p>
                                <p class="mb-0 text-muted text-small w-15 w-sm-100"><?php echo $server['players']; ?> / <?php echo $server['playersmax']; ?></p>
                                <p class="mb-0 text-muted text-small w-15 w-sm-100"><?php echo $server['map']; ?> </p>
                               
                            </div>
                            
                        </div>
                        <button type="button" class="btn btn-outline-success default mb-1">Online</button>
                        <button type="button" class="btn btn-outline-warning default mb-1">Join</button>
                       
                    </div>

         
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </main>
<!-- 

    <footer class="page-footer">
        <div class="footer-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <p class="mb-0 text-muted">ColoredStrategies 2019</p>
                    </div>
                    <div class="col-sm-6 d-none d-sm-block">
                        <ul class="breadcrumb pt-0 pr-0 float-right">
                            <li class="breadcrumb-item mb-0">
                                <a href="#" class="btn-link">Review</a>
                            </li>
                            <li class="breadcrumb-item mb-0">
                                <a href="#" class="btn-link">Purchase</a>
                            </li>
                            <li class="breadcrumb-item mb-0">
                                <a href="#" class="btn-link">Docs</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer> -->








<script src="<?php echo base_url() ?>assets/richcsgo/js/dore.script.js"></script>
<script src="<?php echo base_url() ?>assets/richcsgo/js/scripts.single.theme.js"></script>
</body>