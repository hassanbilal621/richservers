<!--editproduct.php-->
<style>
	th,
	td {
		text-align: center;
		border: 2px #b1a9a9 solid;
		padding: 8px;

	}
</style>
<div class="row">
	<div class="col s12">
		<div class="card">
			<div class="card-content">
				<h4 class="card-title">Edit Product</h4>
				<?php echo form_open_multipart('admin/manageproduct'); ?>
				<div>
					<div class="row">
						<div class="col s12">
							<div class="row">
								<div class="input-field col s12">
									<img src="<?php echo base_url(); ?>assets/uploads/<?php echo $product['pro_img']; ?>" width=300px>
								</div>
							</div>
							<div class="row">
								<div class=" col s12">
									<label for="name2">Product Image</label>
								</div>
								<div class="col s12">
									<input id="img2" name="userfile" value="<?php echo $product['pro_img']; ?>" type="file" accept="image/*">
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<label for="name2">Product Name</label>
								</div>
								<div class="input-field col s12">
									<input id="product2" type="text" name="product" value="<?php echo $product['product']; ?>">
									<input type="hidden" name="productid" value="<?php echo $product['product_id']; ?>">


								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<label for="name2">Supplier Name</label>
								</div>
								<div class="input-field col s12">
									<select class="form-control" name="product_suppliers_id" required style="display: list-item;">
										<?php foreach ($suppliers as $supplier) : ?>
											<?php if ($supplier['suppliers_id'] == $product['product_suppliers_id']) {
													?>
												<option selected value="<?php echo $supplier['suppliers_id']; ?>"><?php echo $supplier['suppliers']; ?></option>
											<?php
												} else {
													?>
												<option value="<?php echo $supplier['suppliers_id']; ?>"><?php echo $supplier['suppliers']; ?></option>
											<?php } ?>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<label for="name2">Category Name</label>
								</div>
								<div class="input-field col s12">
									<select class="form-control" name="product_cat_id" required style="display: list-item;">
										<?php foreach ($categories as $category) : ?>
											<?php if ($category['cat_id'] == $product['product_cat_id']) {
													?>
												<option selected value="<?php echo $category['cat_id']; ?>"><?php echo $category['cat_name']; ?></option>
											<?php
												} else {
													?>
												<option value="<?php echo $category['cat_id']; ?>"><?php echo $category['cat_name']; ?></option>
											<?php } ?>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<table>
							<thead>
								<tr>
									<th>Market Price</th>
									<th>Trade Price</th>
									<th>Pack Size</th>
									<th>General Name</th>
									<th>Discount Distributer on %</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<div class="input-field col s12">
											<input id="mktprice" type="number" name="marketprice" value="<?php echo $product['market_price']; ?>">
										</div>
									</td>
									<td>
										<div class="input-field col s12">
											<input id="trdprice" type="number" name="tradeprice" value="<?php echo $product['trade_price']; ?>">
										</div>
									</td>
									<td>
										<div class="input-field col s12">
											<input id="packsize" type="number" name="packsize" value="<?php echo $product['pack_size']; ?>">
										</div>
									</td>
									<td>
										<div class="input-field col s12">
											<input id="name" type="text" name="generalname" value="<?php echo $product['general_name']; ?>">
										</div>
									</td>
									<td>
										<div class="input-field col s12">
											<input id="discount" type="text" name="discount" value="<?php echo $product['discount']; ?>">
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1  right" type="submit" name="action">submit
								<i class="material-icons right">send</i>
							</button>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>