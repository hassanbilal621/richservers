<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">
                <div class="card">
                    <div class="card-content">
                        <div class="col s12">
                            <h5 class="breadcrumbs-title mt-0 mb-0">Expence Catagory</h5>
                        </div>
                        <?php echo form_open('admin/update_exp_cat'); ?>
                        <div class="input-field col s12">
                            <input type="text" name="exp_cat" placeholder="Add Expence Catagory" value="<?php echo $catagory['exp_cat']; ?>" required>
                            <input type="hidden" name="exp_cat_id" value="<?php echo $catagory['exp_cat_id']; ?>" >
                        </div>
                        <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>