<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<style>
	th, td 
	{
	text-align: center;
	border: 2px #b1a9a9 solid;
	padding: 8px;
	}
</style>
<div class="row">
	<div class="col s12">
		<div class="card">
			<div class="card-content">
				<h4 class="card-title">Add Purchase Order</h4>
				<?php echo form_open_multipart('admin/addpuchaserorder'); ?>
				<div>
					<div class="row">
						<div class="col s6">
							<!-- Form with placeholder -->
							<div class="row">
								<div class="input-field col s12">
									<input list="brow" name="supplier_id" placeholder="Type Supplier Name">
									<datalist id="brow">
										<?php foreach ($suppliers as $supplier): ?>
										<option value="<?php echo $supplier['suppliers']; ?>"></option>
										<?php endforeach?>
									</datalist>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<input type="date" id="img2" name="date" >
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<select id="name6" name="status" style="display: inline-flex;">
										<option value="" disabled selected>Please Select Order Status</option>
										<option value="1">Recived Order</option>
										<option value="2">Panding Order</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row clearfix">
							<div class="col-md-12">
								<table class="table table-bordered table-hover" id="tab_logic">
									<thead>
										<tr>
											<th>S/N</th>
											<th>Product</th>
											<th>Trade Price</th>
											<th>Pack Size</th>
											<th>Qty</th>
											<th>Bonus</th>
											<th>Net Amount</th>
										</tr>
									</thead>
									<tbody>
										<tr id='addr0'>
											<td>1</td>
											<td>
												<select name="product_id" class="form-control" id="" onchange="changevalue(this.value)">
													<?php foreach ($products as $product): ?>
													<option value="<?php echo $product['product_id']; ?>"><?php echo $product['product']; ?></option>
													<?php endforeach?>
												</select>
											</td>
											<td><input type="number" name='Trade_price'value="" id="pricechange" placeholder='Trade Price' class="form-control price" step="0.00" min="0"/></td>
											<td><input type="text" name='pack_size'  placeholder='Pack Size' class="form-control"/></td>
											<td><input type="number" name='qty' placeholder='Enter Qty' class="form-control qty" step="0" min="0"/></td>
											<td><input type="text" name='Bonus'  placeholder='Bonus' class="form-control"/></td>
											<td><input type="number" name='total' placeholder='0.00' class="form-control total" readonly/></td>
										</tr>
										<tr id='addr1'></tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="row clearfix" style="margin-top: 20px;">
							<div class="col-md-12">
								<button id="add_row" class="btn btn-default  left">Add Row</button>
								<button id='delete_row' class=" right btn btn-default">Delete Row</button>
							</div>
						</div>
						<div class="row clearfix" style="margin-top:20px">
							<div class="col align-self-end right">
								<table class="table table-bordered table-hover" id="tab_logic_total">
									<tbody>
										<tr>
											<th class="text-center">Sub Total</th>
											<td class="text-center"><input type="number" name='sub_total' placeholder='0.00' class="form-control" id="sub_total" readonly/></td>
										</tr>
										<tr>
											<th class="text-center">Discount Distributer</th>
											<td class="text-center">
												<div class="input-group mb-2 mb-sm-0">
													<input type="number" class="form-control" id="tax" placeholder="0">
													<div class="input-group-addon">%</div>
												</div>
											</td>
										</tr>
										<tr>
											<th class="text-center">Discount Amount</th>
											<td class="text-center"><input type="number" name='discount_amount' id="tax_amount" placeholder='0.00' class="form-control" readonly/></td>
										</tr>
										<tr>
											<th class="text-center">Grand Total</th>
											<td class="text-center"><input type="number" name='total_amount' id="total_amount" placeholder='0.00' class="form-control" readonly/></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
	    var i=1;
	    $("#add_row").click(function(){b=i-1;
	      	$('#addr'+i).html($('#addr'+b).html()).find('td:first-child').html(i+1);
	      	$('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
	      	i++; 
	  	});
	    $("#delete_row").click(function(){
	    	if(i>1){
			$("#addr"+(i-1)).html('');
			i--;
			}
			calc();
		});
		
		$('#tab_logic tbody').on('keyup change',function(){
			calc();
		});
		$('#tax').on('keyup change',function(){
			calc_total();
		});
		
	
	});
	
	function calc()
	{
		$('#tab_logic tbody tr').each(function(i, element) {
			var html = $(this).html();
			if(html!='')
			{
				var qty = $(this).find('.qty').val();
				var price = $(this).find('.price').val();
				$(this).find('.total').val(qty*price);
				
				calc_total();
			}
	    });
	}
	
	function calc_total()
	{
		total=0;
		$('.total').each(function() {
	        total += parseInt($(this).val());
	    });
		$('#sub_total').val(total.toFixed(2));
		tax_sum=total/100*$('#tax').val();
		$('#tax_amount').val(tax_sum.toFixed(2));
		$('#total_amount').val((tax_sum-total).toFixed(2));
	}
</script>