<div class="row" id="report">
	<table id="page-length-option" class="display">
		<thead>
			<tr>
				<th>#</th>
				<th>Date</th>
				<th>Company Name</th>
				<th>Supplier Name</th>
				<th>Order By</th>
				<th>Order Note</th>
				<th>Grand Total</th>
				<th>Order Status</th>
			</tr>
		</thead>

		<?php if (isset($invoicess)) {
			?>
			<tbody>
				<?php foreach ($invoicess as $invoice) : ?>
					<tr>
						<td><?php echo $invoice['invoice_id']; ?></td>
						<td><?php echo $invoice['invoicedate']; ?></td>
						<td><?php echo $invoice['companyname']; ?></td>
						<td><?php echo $invoice['customer_name']; ?></td>
						<td><?php echo $invoice['order_by']; ?></td>
						<td><?php echo $invoice['order_note']; ?></td>
						<td><?php echo $invoice['grand_total']; ?></td>
						<td><?php echo $invoice['status']; ?></td>
					</tr>
				<?php endforeach; ?>
				</tfoot>

			<?php } else {
				echo "No Data Available ";
			} ?>
	</table>
</div>