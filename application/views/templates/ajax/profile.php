<div id="card-widgets">
    <div class="row">
        <div class="col s12">
            <div id="profile-card" class="card animate fadeRight">
                <div class="card-image waves-effect waves-block waves-light">
                <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $supplier['pro_img']; ?>" alt="user bg" />
                </div>
                <div class="card-content">
                    
                    <p><i class="material-icons profile-card-i">perm_identity</i> <?php echo $supplier['suppliers']; ?></p>
                    <p><i class="material-icons profile-card-i">email</i> <?php echo $supplier['email']; ?></p>
                    <a class="btn-floating activator btn-move-up waves-effect waves-light submit accent-2 z-depth-4 right"> More Info
                    </a>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4"> <i class="material-icons right">close</i>
                    </span>
                    <p>Here is some more information about Supplier.</p>
                    <p><i class="material-icons">perm_identity</i> <?php echo $supplier['suppliers']; ?></p>
                    <p><i class="material-icons">email</i> <?php echo $supplier['email']; ?></p>
                    <p></p>
                    <p><i class="material-icons">airplanemode_active</i><?php echo $supplier['address']; ?></p>
                    <p></p>
                    <p>Supplier Have only <?php echo $total_supplier_product; ?> Products </p>

                </div>
            </div>
        </div>
    </div>
</div>
