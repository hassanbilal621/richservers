<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <?php echo form_open('admin/managecompany') ?>
                <div class="col s12">
                    <!-- Form with placeholder -->
                    <h4 class="card-title">Edit Company</h4>
                    <div class="row">
                        <div class="input-field col s12">
                           <label for="name2">Company Name</label>
                        </div>
                        <div class="input-field col s12">
                            <input id="name2" type="text" value="<?php echo $company['companyname']; ?>" name="companyname">
                            <input type="hidden" name="companyid" value="<?php echo $company['company_id']; ?>" >
                        </div>
                    </div>
                    
                    <div class="row">
                         <div class="input-field col s12">
                           <label for="address">Company Address</label>
                        </div>
                        <div class="input-field col s12">
                            <input id="address" type="text" value="<?php echo $company['companyaddress']; ?>" name="address">
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <select name="status" class="browser_default">
                                <?php if ($company['status'] = 1 )
                                {
                                ?>
                                    <option selected value="1">Active</option>
                                    <option  value="2">Deactive</option>
                                <?php 
                                }
                                else 
                                {
                                ?>
                                    <option  value="1">Active</option>
                                    <option selected value="2">Deactive</option>
                                <?php 
                                }
                                ?>
                            </select>
                         </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1  right" type="submit" name="action">Save
                            <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close() ?>
        </div>
    </div>
</div>