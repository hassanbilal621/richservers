<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="col s12">
                    <!-- Form with placeholder -->
                    <h4 class="card-title">Edit Suppliers</h4>
                    <?php echo form_open_multipart('admin/managesuppliers') ?>
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="name2">Supplier image</label>
                        </div>
                        <div class="input-field col s12">
                            <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $supplier['pro_img']; ?>" width=300px" />

                            <input id="img2" type="file" name="userfile" accept="image/*">

                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="supplier ">Supplier Name</label>
                        </div>
                        <div class="input-field col s12">
                            <input id="suppliers" type="text" value="<?php echo $supplier['suppliers']; ?>" name="suppliers">
                            <input type="hidden" name="supplierid" value="<?php echo $supplier['suppliers_id']; ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="email">Supplier Email</label>
                        </div>
                        <div class="input-field col s12">
                            <input id="email" type="text" value="<?php echo $supplier['email']; ?>" name="email">
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="address">Supplier Address</label>
                        </div>
                        <div class="input-field col s12">
                            <input id="address" type="text" value="<?php echo $supplier['address']; ?>" name="address">
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" type="submit" name="action">submit
                                <i class="material-icons right">mode_edit</i>
                            </button>
                        </div>
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>