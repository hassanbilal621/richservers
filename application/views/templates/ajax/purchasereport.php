<div class="row" id="report">
	<table id="page-length-option" class="display">
		<thead>
			<tr>
				<th>#</th>
				<th>Date</th>
				<th>Supplier Name</th>
				<th>No_of_P roduct</th>
				<th>Discount</th>
				<th>Grand Total</th>
				<th>Order Status</th>
			</tr>
		</thead>

		<?php if (isset($purchaseorders)) {
			?>
			<tbody>
				<?php foreach ($purchaseorders as $purchaseorder) : ?>
					<tr>
						<td><?php echo $purchaseorder['purchase_order_id']; ?></td>
						<td><?php echo $purchaseorder['date']; ?></td>
						<td><?php echo $purchaseorder['supplier_name_id']; ?></td>
						<td><?php echo $purchaseorder['sub_total']; ?></td>
						<td><?php echo $purchaseorder['discount']; ?></td>
						<td><?php echo $purchaseorder['grand_total']; ?></td>
						<td><?php echo $purchaseorder['status']; ?></td>
					</tr>
				<?php endforeach; ?>
				</tfoot>

			<?php } else {
				echo "No Data Available ";
			} ?>
	</table>
</div>