<div class="row">
   <div class="col s12">
      <div class="container">
         <div class="section">
            <div class="card">
               <div class="card-content">
                  <div class="col s12">
                     <h5 class="breadcrumbs-title mt-0 mb-0">Expence Catagory</h5>
                  </div>
                  <?php echo form_open('admin/manageexpence'); ?>
                  <div class="input-field col s6">
                     <select name="expense_cat_id" class="browser-default">
                        <option disabled selected value="">Select Expence Catagory </option>
                        <?php foreach ($categories as $catagory) : ?>
                        <?php if($expence['expense_cat_id'] == $catagory['exp_cat']) 
                        {
                        ?>
                           <option selected value="<?php echo $catagory['exp_cat_id']; ?>"><?php echo $catagory['exp_cat']; ?></option>
                        <?php
                        }
                        else {
                        ?>
                           <option value="<?php echo $catagory['exp_cat_id']; ?>"><?php echo $catagory['exp_cat']; ?></option>
                        <?php } 
                        ?>
                        <?php endforeach; ?>
                     </select>
                  </div>
                  <div class="input-field col s6">
                     <input type="number" name="expence_price" placeholder="Add Expence Price" value="<?php echo $expence['expence_price']; ?>" required>
                     <input type="hidden" name="expenceid" value="<?php echo $expence['expense_id']; ?>">
                  </div>
                  <div class="input-field col s12">
                     <input type="text" name="expence_note" placeholder="Add Expence Note" value="<?php echo $expence['expence_note']; ?>" required>
                  </div>
                  <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Submit
                     <i class="material-icons right">send</i>
                  </button>
               </div>
               <?php echo form_close(); ?>
            </div>
         </div>
      </div>
   </div>
</div>