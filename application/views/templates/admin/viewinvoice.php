<style>
   span {
      color: #272361 !important;
   }
</style>
<div id="main">
   <div class="row">
      <div class="col s12 m12 l12">
         <div id="basic-tabs" class="card card card-default scrollspy">
            <div class="card-content pt-5 pr-5 pb-5 pl-5">
               <div id="invoice">
                  <div class="invoice-header">
                     <div class="row section">
                        <div class="col s12 m6 l6">
                           <h6 class="text-uppercase strong"><?php echo $invoice['companyname']; ?></h6>
                           <p><?php echo $invoice['companyaddress']; ?></p>
                        </div>
                        <div class="col s12 m6 l6">
                           <h4 class="text-uppercase right-align strong mb-5">Invoice</h4>
                        </div>
                     </div>
                     <div class="row section">
                        <div class="col s12 m6 l6">
                           <h6 class="text-uppercase strong mb-2 mt-3">Recipient</h6>
                           <p class="text-uppercase"><?php echo $invoice['customer_name']; ?></p>
                           <p><?php echo $invoice['billing_address']; ?></p>
                        </div>
                        <div class="col s12 m6 l6">
                           <div class="invoce-no right-align">
                              <p><span class="text-uppercase strong">Invoice No.</span> <?php echo $invoice['invoice_id']; ?></p>
                           </div>
                           <div class="invoce-no right-align">
                              <p><span class="text-uppercase strong">Invoice Date:</span> <?php echo $invoice['invoicedate']; ?></p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="mt-5 invoice-table">
                     <div class="row">
                        <div class="col s12 m12 l12">
                           <table class="highlight responsive-table">
                              <thead>
                                 <tr>
                                    <th>S/N</th>
                                    <th>product Id</th>
                                    <th>Price</th>
                                    <th>Qty</th>
                                    <th>Discount</th>
                                    <th>Net Amount</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php $s_n = 1; ?>
                                 <?php foreach ($invoiceitems as $invoiceitem) : ?>
                                    <tr>
                                       <td><?php echo $s_n; ?></td>
                                       <td><?php echo $invoiceitem['product_name_id']; ?></td>
                                       <td><?php echo $invoiceitem['market_price']; ?></td>
                                       <td><?php echo $invoiceitem['Qty']; ?></td>
                                       <td><?php echo $invoiceitem['discount']; ?></td>
                                       <td><?php echo $invoiceitem['netamount']; ?></td>
                                    </tr>

                                 <?php
                                    $s_n++;
                                 endforeach; ?>

                                 <tr class="border-none">
                                    <td colspan="4"></td>
                                    <td>Sub Total:</td>
                                    <td>RS:<?php echo $invoice['grand_total']; ?></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <div class="invoice-footer mt-6">
                     <div class="row">
                        <div class="col s12 m6 l6">
                           <p class="strong">Customer Note</p>
                           <p><?php echo $invoice['order_note']; ?></p>
                           <p class="strong">Order By:<?php echo $invoice['order_by']; ?></p>
                           
                        </div>
                        <div class="col s12 m6 l6 center-align">
                           <p>Approved By</p>
                           <P class="mt-10"></P>
                           <p class="header">Amzi Enterprices</p>
                           <p>Managing Director</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>