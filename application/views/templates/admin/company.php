<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <h1 class="card-title"><?php echo $company['companyname']; ?></h1>
                    <div class="row">
                        <div id="editcompanydiv" class="col s12" style="display: none;">
                            <?php echo form_open('admin/managecompany') ?>
                            <div class="row">
                                <div class="col s6">
                                    <div class="input-field col s12">
                                        <label for="name2">Edit Company name</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="name2" type="text" value="<?php echo $company['companyname']; ?>" name="companyname">
                                        <input type="hidden" name="companyid" value="<?php echo $company['company_id']; ?>">
                                    </div>
                                </div>

                                <div class="col s6">
                                    <div class="input-field col s12">
                                        <label for="name2">Edit Company Address</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="address" type="text" value="<?php echo $company['companyaddress']; ?>" name="address">
                                    </div>
                                </div>
                            </div>
                            <div id="savecompanybtn" class="row">
                                <div class="input-field col s12">
                                    <button class="waves-effect waves-light  btn edit box-shadow-none border-round mr-1 mb-1  right" type="submit" name="action">Save
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>
                            </div>
                            <?php echo form_close() ?>
                        </div>
                        <div id="editcompanybtn" class="row">
                            <div class="input-field col s12">
                                <button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1" type="submit">Edit company</button>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <table id="page-length-option" class="display">
                            <thead>
                                <tr>
                                    <th>Invoice id</th>
                                    <th>Date</th>
                                    <th>Customer Name</th>
                                    <th>Order By</th>
                                    <th>Order Note</th>
                                    <th>Grand Total</th>
                                    <th>View</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($invoices as $invoice) : ?>
                                    <tr>
                                        <td><?php echo $invoice['invoice_id']; ?></td>
                                        <td><?php echo $invoice['invoicedate']; ?></td>
                                        <td><?php echo $invoice['customer_id']; ?></td>
                                        <td><?php echo $invoice['order_by']; ?></td>
                                        <td><?php echo $invoice['companyid']; ?></td>
                                        <td><?php echo $invoice['order_note']; ?></td>
                                        <td><?php echo $invoice['grand_total']; ?></td>
                                        <td>
                                            <a class="waves-effect waves-light  btn  submit box-shadow-none border-round mr-1 mb-1" href="<?php echo base_url(); ?>admin/viewinvoice/<?php echo $invoice['invoice_id']; ?>" type="submit" name="action">View Invoice
                                                <i class="material-icons left">visibility</i>
                                            </a>

                                        </td>
                                        <td></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div id="modal3" class="modal">
    <div class="modal-content">
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>
    function loadinvoice(invoiceid) {
        // var userid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_Invoice/" + invoiceid,
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal3').modal('open');
            }
        });
    }
</script>
<script>
    function del(id) {

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            icon: 'warning',
            dangerMode: true,
            buttons: {
                cancel: 'No, Please!',
                delete: 'Yes, Delete It'
            }
        }).then(function(willDelete) {
            if (willDelete) {
                var html = $.ajax({
                    type: "GET",
                    url: "<?php echo base_url(); ?>admin/deleteproduct/" + id,
                    // data: info,
                    async: false
                }).responseText;

                if (html == "success") {
                    $("#delete").html("delete success.");
                    return true;

                }
                swal("Poof! Your record has been deleted!", {
                    icon: "success",
                });
                setTimeout(location.reload.bind(location), 1000);
            } else {
                swal("Your imaginary file is safe", {
                    title: 'Cancelled',
                    icon: "error",
                });
            }
        });


    }
</script>
<script>
    $(document).ready(function() {
        $("#editcompanybtn").click(function() {
            $("#editcompanydiv").show();
            $("#editcompanybtn").hide();
        });
    });
</script>
<script>
    $(document).ready(function() {
        $("#savecompanybtn").click(function() {
            $("#editcompanydiv").hide();
            $("#editcompanybtn").show();
        });
    });
</script>