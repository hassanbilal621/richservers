<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">
                     <div class="col s10">
                        <h4 class="card-title">Add Customer</h4>
                     </div>
                     <div class="col s2 right">
                        <label for="date">Date</label>
                        <input type="date" name="date">
                     </div>
                  </div>
                  <div class="row">
                     <?php echo form_open('admin/addcustomer') ?>
                     <div class="col s6">
                        <div class="row">
                           <div class="input-field col s12">
                              <input id="customer2" type="text" name="customer_name">
                              <label for="customer2">Customer</label>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <input id="address2" type="text" name="billing_address">
                              <label for="address2">Billing Address</label>
                           </div>
                        </div>

                        <div class="row">
                           <div class="input-field col s12">
                              <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1" type="submit" name="action">submit
                                 <i class="material-icons right">send</i>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php echo form_close() ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>