<div id="main">
  <div class="row">
    <!-- Page Length Options -->
   
       

   
      
                
               
 <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content">

              
               
    

                
               

          <h4 class="card-title">Users</h4>
            
            <div class="row">
              <div class="col s12">

              <button class="waves-effect waves-light btn modal-trigger mb-2 mr-1" href="#modal1">Add
              <i class="material-icons left">person_add</i>
              </button>
                
                <div id="modal1" class="modal">
                <div class="modal-content">
                    
               
              
                <div class="row">
    <div class="col s12">
      <div class="card">
                <form class="col s12">
                    <!-- Form with placeholder -->
            
                <h4 class="card-title">Form with placeholder</h4>
                   
                    <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="John Doe" id="name2" type="text">
                        <label for="name2">Name</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Johar" id="address2" type="text">
                        <label for="address2">Address</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Karachi" id="city2" type="text">
                        <label for="city2">City</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Pakistan" id="country2" type="text">
                        <label for="country2">Pakistan</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="+920000000000" id="phone2" type="text">
                        <label for="phone2">Phone</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="john@domainname.com" id="email2" type="email">
                        <label for="email">Email</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Username" id="username2" type="text">
                        <label for="username2">Username</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="YourPassword" id="password2" type="password">
                        <label for="password">Password</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <textarea placeholder="John Doe" id="fathername2" class="materialize-textarea"></textarea>
                        <label for="fathername2">Father Name</label>
                    </div>
            </div>
            <div class="row">
            
              <div class="input-field col s12">
              <input id="picture2" type="file">
                
              </div>
            </div>
              <div class="row">
                <div class="input-field col s12">
                  <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
              </div>
             
          </form>
        </div>
        </div>
        </div>
              </div>
              </div>


                <table id="page-length-option" class="display">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Position</th>
                      <th>Office</th>
                      <th>Age</th>
                      <th>Start date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System Architect</td>
                      <td>Edinburgh</td>
                      <td>61</td>
                      <td>2011/04/25</td>
                      <td>  <button class="btn waves-effect waves-light blue " type="submit" name="action">Edit
    <i class="material-icons left">edit</i>
  </button>
  <button class="btn waves-effect waves-light red  " type="submit" name="action">Delete
    <i class="material-icons left">delete_forever</i>
   
  </button></td>
                    </tr>
                    <tr>
                      <td>Garrett Winters</td>
                      <td>Accountant</td>
                      <td>Tokyo</td>
                      <td>63</td>
                      <td>2011/07/25</td>
                      <td>  <button class="btn waves-effect waves-light blue " type="submit" name="action">Edit
    <i class="material-icons left">edit</i>
  </button>
  <button class="btn waves-effect waves-light red  " type="submit" name="action">Delete
    <i class="material-icons left">delete_forever</i>
   
  </button></td>
                    </tr>
                   
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
   
  </div>
  </div>
  </div>
   
 <!-- BEGIN VENDOR JS-->
 <script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->