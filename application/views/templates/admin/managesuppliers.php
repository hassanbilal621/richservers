<div id="main">
	<div class="row">
		<!-- Page Length Options -->
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content">
						<h4 class="card-title">manage Supplier</h4>
						<div class="row">
							<table id="page-length-option" class="display">
								<thead>
									<tr>
										<th>#</th>
										<th>Suppliers</th>
										<th>Supplier Email</th>
										<th>Image</th>
										<th>Address</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($suppliers as $supplier): ?>
									<tr>
										<td><?php echo $supplier['suppliers_id']; ?></td>
										<td><?php echo $supplier['suppliers']; ?></td>
										<td><?php echo $supplier['email']; ?></td>
										<td><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $supplier['pro_img']; ?>" width="64px"/> </td>
										<td><?php echo $supplier['address']; ?></td>
										<td>
											<button class="waves-effect waves-light btn edit z-depth-2 mb-1 ml-1 edit modal-trigger" onclick="loadsupplierinfo(this.id)" id="<?php echo $supplier['suppliers_id']; ?>" type="submit"  href="#modal3"  name="action">EDIT
											<i class="material-icons left">edit</i>
											</button>
											<a class="waves-effect waves-light btn delete z-depth-2 mb-1 ml-1" id="<?php echo $supplier['suppliers_id']; ?>" onclick="del(this.id)" type="submit" name="action">DELETE
											<i class="material-icons left">delete_forever</i>
											</a>
										</td>
									</tr>
									<?php endforeach; ?>
									</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="modal3" class="modal">
	<div class="modal-content">
	</div>
</div>
<script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>
	function loadsupplierinfo(supplierid)
	{
	   // var userid = this.id;
	      $.ajax({
	         type: "GET",
	         url: "<?php echo base_url();?>admin/ajax_edit_supplier_adminmodal/"+supplierid,
	         success: function(data){
	            $(".modal-content").html(data);
	            $('#modal3').modal('open');
	         }
	      });
	}
</script>
<script>
	function del(id){
	
	        swal({
	            title: "Are you sure?",
	            text: "You will not be able to recover this imaginary file!",
	            icon: 'warning',
	            dangerMode: true,
	            buttons: {
	            cancel: 'No, Please!',
	            delete: 'Yes, Delete It'
	            }
	        }).then(function (willDelete) {
	            if (willDelete) {
	                var html = $.ajax({
	                type: "GET",
	                url: "<?php echo base_url();?>admin/suppliersdelete/"+id,
	                // data: info,
	                async: false
	                }).responseText;
	
	                if(html == "success")
	                {
	                    $("#delete").html("delete success.");
	                    return true;
	
	                }
	                swal("Poof! Your record has been deleted!", {
	                    icon: "success",
	                });
	                setTimeout(location.reload.bind(location), 1000);
	                } else {
	                swal("Your imaginary file is safe", {
	                    title: 'Cancelled',
	                    icon: "error",
	                });
	                }
	        });  
	
	
	}
	
	
</script>