<style>
th, td 
{
   text-align: center;
   border: 1px #b1a9a9 solid;
	padding: 8px;

}

</style>
<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Manage Product</h4>
                  <div class="row">
                     
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Products</th>
                              <th>image</th>
                              <th>catagory</th>
                              <th>supplier Name</th>
                              <th>Market Price</th>
                              <th>Trade Price</th>
                              <th>Pack Size</th>
                              <th>General Name</th>
                              <th>Discount Distributer on %</th>
                              <th>Action</th>
                              <th></th>
                           </tr>
                        </thead>
                        <tbody>
                        <?php foreach($products as $product): ?>
                           <tr>
                              <td><?php echo $product['product_id']; ?></td>
                              <td><?php echo $product['product']; ?></td>
                              <td><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $product['pro_img']; ?>" width="64px"></td>
                              <td><?php echo $product['suppliers']; ?></td>
                              <td><?php echo $product['cat_name']; ?></td>
                              <td><?php echo $product['market_price']; ?></td>
                              <td><?php echo $product['trade_price']; ?></td>                              
                              <td><?php echo $product['pack_size']; ?></td>
                              <td><?php echo $product['general_name']; ?></td>
                              <td><?php echo $product['discount']; ?></td>
                              
                              
                              <td>
											<button class="waves-effect waves-light  btn red box-shadow-none border-round mr-1 mb-1 edit modal-trigger" onclick="loadproductinfo(this.id)" id="<?php echo $product['product_id']; ?>" type="submit"  href="#modal3"  name="action">EDIT
											<i class="material-icons left">edit</i>
											</button>
                                 <a class="waves-effect waves-light  btn red box-shadow-none border-round mr-1 mb-1 " id="<?php echo $product['product_id']; ?>" onclick="del(this.id)" type="submit" name="action">DELETE
											<i class="material-icons left">delete_forever</i>
                                 
                              </td>
                              <td></td>
                           </tr>
                           <?php endforeach;?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="modal3" class="modal">
    <div class="modal-content">
    </div>
</div>
   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
   <script>
      function loadproductinfo(productid)
      {
         // var userid = this.id;
            $.ajax({
               type: "GET",
               url: "<?php echo base_url();?>admin/ajax_edit_product_adminmodal/"+productid,
               success: function(data){
                  $(".modal-content").html(data);
                  $('#modal3').modal('open');
               }
            });
      }
   </script>
<script>
	function del(id){
	
	        swal({
	            title: "Are you sure?",
	            text: "You will not be able to recover this imaginary file!",
	            icon: 'warning',
	            dangerMode: true,
	            buttons: {
	            cancel: 'No, Please!',
	            delete: 'Yes, Delete It'
	            }
	        }).then(function (willDelete) {
	            if (willDelete) {
	                var html = $.ajax({
	                type: "GET",
	                url: "<?php echo base_url();?>admin/deleteproduct/"+id,
	                // data: info,
	                async: false
	                }).responseText;
	
	                if(html == "success")
	                {
	                    $("#delete").html("delete success.");
	                    return true;
	
	                }
	                swal("Poof! Your record has been deleted!", {
	                    icon: "success",
	                });
	                setTimeout(location.reload.bind(location), 1000);
	                } else {
	                swal("Your imaginary file is safe", {
	                    title: 'Cancelled',
	                    icon: "error",
	                });
	                }
	        });  
	
	
	}
	
	
</script>