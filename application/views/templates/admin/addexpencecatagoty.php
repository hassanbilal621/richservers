<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="card">
                        <div class="card-content">
                        <div class="row">
							<div class="col s10">	
                               <h4 class="card-title">Add Expence Categories</h4>
							</div>
							<div class="col s2 right">
								<label for="date">Date</label>
								<input type="date" name="date">
							</div>
						</div>
                            <?php echo form_open('admin/addexpencecatagoty'); ?>
                            <div class="input-field col s12">
                                <input type="text" name="exp_cat" placeholder="Add Expence Catagory" required>
                            </div>
                            <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1" id="view" type="submit" name="action">Submit
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
		<!-- Page Length Options -->
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content">
						<h4 class="card-title">Manage Expence catagory</h4>
						<div class="row">
							<table id="page-length-option" class="display">
								<thead>
									<tr>
										<th>Cat id</th>
										<th>Catagory Name</th>
                                        <th>status</th>
                                        <th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($categories as $catagory) : ?>
									<tr>
										<td><?php echo $catagory['exp_cat_id']; ?></td>
                                        <td><?php echo $catagory['exp_cat']; ?></td>
                                        <td><?php echo $catagory['status']; ?></td>
                                        
                                        <td> <?php if($catagory['status'] == 'deactive')
                                            {
                                        ?>
                                            <a class="waves-effect waves-light  btn gradient-45deg-green-teal box-shadow-none border-round mr-1 mb-1" onclick="active()" href="<?php echo base_url(); ?>admin/activestatus/<?php echo $catagory['exp_cat_id']; ?>" type="submit" name="action">Active
                                                <i class="material-icons left">done</i>
                                            </a>
                                        <?php
                                            }
                                            else
                                            {
                                        ?>
                                            <a class="waves-effect waves-light  btn red darken-4 box-shadow-none border-round mr-1 mb-1"  onclick="deactive()" href="<?php echo base_url(); ?>admin/deactivestatus/<?php echo $catagory['exp_cat_id']; ?>" type="submit" name="action">Deactive
                                                <i class="material-icons left">cancel</i>
                                            </a>
                                        <?php
                                             }
                                        ?>
                                           
                                           
											<button class="waves-effect waves-light  btn edit box-shadow-none border-round mr-1 mb-1 modal-trigger" onclick="loadcatagoryinfo(this.id)" id="<?php echo $catagory['exp_cat_id']; ?>" type="submit"  href="#modal3"  name="action">EDIT
												<i class="material-icons left">edit</i>
											</button>
                                            <a class="waves-effect waves-light  btn delete box-shadow-none border-round mr-1 mb-1" id="<?php echo $catagory['exp_cat_id']; ?>" onclick="del(this.id)" type="submit" name="action">DELETE
                                                <i class="material-icons left">delete_forever</i>
                                            </a>
                                           
                                        </td>
									</tr>
									<?php endforeach; ?>
									</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div id="modal3" class="modal">
    <div class="modal-content">
    </div>
</div>
   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
   <script>
      function loadcatagoryinfo(catagoryid)
      {
         // var userid = this.id;
            $.ajax({
               type: "GET",
               url: "<?php echo base_url();?>admin/ajax_edit_catagory_adminmodal/"+catagoryid,
               success: function(data){
                  $(".modal-content").html(data);
                  $('#modal3').modal('open');
               }
            });
      }
   </script>
<script>
	function del(id){
	
	        swal({
	            title: "Are you sure?",
	            text: "You will not be able to recover this imaginary file!",
	            icon: 'warning',
	            dangerMode: true,
	            buttons: {
	            cancel: 'No, Please!',
	            delete: 'Yes, Delete It'
	            }
	        }).then(function (willDelete) {
	            if (willDelete) {
	                var html = $.ajax({
	                type: "GET",
	                url: "<?php echo base_url();?>admin/cat_delete/"+id,
	                // data: info,
	                async: false
	                }).responseText;
	
	                if(html == "success")
	                {
	                    $("#delete").html("delete success.");
	                    return true;
	
	                }
	                swal("Poof! Your record has been deleted!", {
	                    icon: "success",
	                });
	                setTimeout(location.reload.bind(location), 1000);
	                } else {
	                swal("Your imaginary file is safe", {
	                    title: 'Cancelled',
	                    icon: "error",
	                });
	                }
	        });  
	
	
	}
	
	
</script>

<script>
function deactive(){
    
  swal({
    title: 'Deactive catagory',
    icon: 'error',
    buttons: false
  });
}

</script>

<script>
function active(){
    
  swal({
    title: 'Active catagory',
    icon: 'success',
    buttons: false
  });
}

</script>

