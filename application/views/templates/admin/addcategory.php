<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">
							<div class="col s10">	
                     <h4 class="card-title">Add Categories</h4>
							</div>
							<div class="col s2 right">
								<label for="date">Date</label>
								<input type="date" name="date">
							</div>
						</div>
                  <div class="row">
                  <?php echo form_open('admin/addcategory') ?>
                     <div class="col s12">
                        <!-- Form with placeholder -->
                        <h6 class="card-title">use this form to add Categories to database..</h6>
                        <div class="row">
                           <div class="input-field col s12">
                              <input id="name2" type="text" name="cat_name">
                              <label for="name2">Add Categories</label>
                           </div>
                        </div>
                       
                        <div class="row">
                           <div class="input-field col s12">
                              <button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">submit
                              <i class="material-icons right">send</i>
                              </button>
                           </div>
                        </div>
                  </div>
                  </div>
                  <?php echo form_close() ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->