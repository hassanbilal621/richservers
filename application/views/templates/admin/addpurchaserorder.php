<div id="main">
	<div class="row">
		<div class="col s12">
			<?php echo form_open('admin/addpurchaseorder'); ?>
			<div class="card">
				<div class="card-content">
					<div class="row">
						<div class="col s10">
							<h4 class="card-title">Add Your Purchase Order</h4>
						</div>
						<div class="col s2 right">
							<label for="date">Date</label>
							<input type="text" class="datepicker" name="date">
						</div>
					</div>
					<div class="box box-block bg-white">
						<div class="row">
							<div class="input-field col s6">
								<select name="supplier" required>
										<option disabled selected value="">Select Supplier </option>
									<?php foreach ($suppliers as $supplier) : ?>
										<option value="<?php echo $supplier['suppliers_id']; ?>"><?php echo $supplier['suppliers']; ?></option>
									<?php endforeach ?>
								</select>
							</div>
							<div class="input-field col s6">
								<input id="mobile2" name="order_by " type="text" required>
								<label for="mobile2">Order by</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s6">
								<input id="address2" name="order_note" type="text" required>
								<label for="address2">Order Note For Supplier</label>
							</div>
							<div class="input-field col s6">
								<input id="date2" class="datepiker" name="order_datetime" type="date" required>
								<label for="date2">Date</label>
							</div>
						</div>
					</div>
				</div>
				<div class="card-content">
					<div class="box box-block bg-white">
						<div class="row">
							<div class="col s6">
								<select name="product_id" id="product_id" onchange="changevalue(this.value)">
									<option disable selected value="">Select Product</option>
									<?php foreach ($products as $product) : ?>
										<option value="<?php echo $product['product_id']; ?>">
											<?php echo $product['product']; ?>
										</option>
									<?php endforeach ?>
								</select>
							</div>
							<div class="col s6">
								<P class="right" style="margin: 30px 0 0 0;">If you want yo add new product <a href="<?php echo base_url(); ?>admin/addproduct""> Click here</a> </P>
							</div>
						</div>
						<div class=" row">
										<div class="input-field col s2">
											<input type="number" name='trade_price' value="" id="tradeprice" placeholder='0.00' class="form-control price" step="0.00" min="0" readonly />
											<label for="trade_price">Trade Price</label>
										</div>
										<div class="input-field col s2">
											<input onkeyup="onpay(this.value)" name='qty' id="qty" placeholder='0.00' class="qty validate" step="0" min="1" type="number" pattern="[0-9]*" />
											<label for="qty">Qty</label>
										</div>
										<div class="input-field col s2">
											<input type="number" name='amount' id="amount" value="" placeholder='0.00' class="form-control total" readonly />
											<label for="amount">Amount</label>
										</div>
										<div class="input-field col s2">
											<input type="number" name='discount' onkeyup="calculatediscount(this.value)" id="discount" value="" placeholder='0.00' class="form-control total" />
											<label for="discount">Discount</label>
										</div>
										<div class="input-field col s2">
											<input type="number" name='totalamount' id="totalamount" value="" placeholder='0.00' class="form-control total" readonly />
											<label for="totalamount">Total Amount</label>
										</div>
										<div class="input-field col s2">
											<a id="add_row" class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1">Add
												Product</a>
										</div>
										<table class="table table-bordered table-hover" id="tab_logic">
											<thead>
												<tr>
													<th>Product</th>
													<th>Trade Price</th>
													<th>Qty</th>
													<th>Discount</th>
													<th>Net Amount</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody id="dynamic_field">
											</tbody>
										</table>
										<div class="row clearfix" style="margin-top:20px">
											<div class="col-md-12 right">
												<table class="table table-bordered table-hover" id="tab_logic_total">
													<tbody>
														<tr>
															<th class="text-center">Grand Total</th>
															<td class="text-center"><input type="number" name='total_amount' id="total_amount" placeholder='0.00' class="form-control" readonly />
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="row clearfix" style="margin-top: 20px;">
											<div class="col-md-12">
												<button type="submit" class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right">Submit
													Order</button>
											</div>
										</div>
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
	<script>
		function calculatediscount(discountamount) {
			var finalamount = Number(discountamount) / 100;
			var getamount = Number(document.getElementById("amount").value) * finalamount;
			var calculatedamount = Number(document.getElementById("amount").value) - getamount;
			document.getElementById("totalamount").value = calculatedamount;

		}
	</script>
	<script>
		function trprice(trprice) {
			document.getElementById("qty").value = 1;
			document.getElementById("amount").value = trprice;

		}

		function onpay(qty) {
			var payamount = document.getElementById("tradeprice").value;
			var total = Number(qty) * Number(payamount);
			document.getElementById("amount").value = total;
			discountamount = document.getElementById("discount").value;
			var finalamount = Number(discountamount) / 100;
			var getamount = Number(document.getElementById("amount").value) * finalamount;
			var calculatedamount = Number(document.getElementById("amount").value) - getamount;
			document.getElementById("totalamount").value = calculatedamount;


		}
	</script>
	<script>
		function changevalue(productid) {
			$.ajax({
				type: "GET",
				url: "<?php echo base_url(); ?>admin/ajax_get_product_details/" + productid,
				success: function(data) {
					var obj = JSON.parse(data);
					document.getElementById("tradeprice").value = obj.trade_price;
					document.getElementById("qty").value = 1;
					document.getElementById("amount").value = obj.trade_price;
					document.getElementById("discount").value = obj.discount;
					var finalamount = Number(obj.discount) / 100;
					var getamount = Number(obj.trade_price) * finalamount;
					var calculatedamount = Number(obj.trade_price) - getamount;
					document.getElementById("totalamount").value = calculatedamount;
				}
			});
		}
	</script>
	<script>
		$(document).ready(function() {
			var finaltotal = 0;
			var i = 1;
			$('#add_row').click(function() {
				var e = document.getElementById("product_id");
				var productname = e.options[e.selectedIndex].text;
				var productid = document.getElementById("product_id").value;
				var tradeprice = document.getElementById("tradeprice").value;
				var qty = document.getElementById("qty").value;
				var total = document.getElementById("amount").value;
				var discount = document.getElementById("discount").value;
				var totalamount = document.getElementById("totalamount").value;
				finaltotal = Number(finaltotal) + Number(totalamount);
				document.getElementById("total_amount").value = finaltotal;
				i++;
				$('#dynamic_field').append('<tr id="row' + i + '"><td>' + productname +
					'<input type="hidden" name="products[]" value="' + productid +
					'" id="Product" placeholder="Product Name" class="form-control Product" /></td><td><input type="number" name="tradeprice[]" value="' +
					tradeprice +
					'" readonly placeholder="Trade Price" class="form-control price" step="0.00" min="0" /></td><td><input type="number" name="qty[]" placeholder="Enter Qty" value="' +
					qty +
					'" class="form-control qty" readonly step="0" min="0" /></td><td><input type="number" name="discount[]" readonly placeholder="0.00" class="form-control total" value="' +
					discount + '" readonly /></td><td><input type="number" name="total[]" id="totalamount' +
					i + '" placeholder="0.00" class="form-control total" value="' + totalamount +
					'" readonly /></td><td><a name="remove" id="' + i +
					'" class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1  btn submit box-shadow-none border-round mr-1 mb-1 right btn_remove">Delete Row</a> </td></tr>'
				);
			});
			$(document).on('click', '.btn_remove', function() {
				var button_id = $(this).attr("id");
				getrowvalue = document.getElementById("totalamount" + button_id).value;
				alert(getrowvalue);
				finaltotal = finaltotal - Number(getrowvalue);
				document.getElementById("total_amount").value = finaltotal;
				$('#row' + button_id + '').remove();
			});
		});
	</script>
	<script>
		$(document).ready(function() {
			$('.datepicker').datepicker();
		});
	</script>