<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card">
            <div class="card-content">
               <h4 class="card-title">Manage Product</h4>
               <div class="row">
                  <table id="page-length-option" class="display">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Date</th>
                           <th>Supplier Name</th>
                           <th>No_of_Product</th>
                           <th>Grand Total</th>
                           <th>Order Status</th>
                           <th>Action</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php foreach ($purchaseorders as $purchaseorder) : ?>
                           <tr>
                              <td><?php echo $purchaseorder['purchase_order_id']; ?></td>
                              <td><?php echo $purchaseorder['date']; ?></td>
                              <td><?php echo $purchaseorder['suppliers']; ?></td>
                              <?php
                                 $purchaseorderid = $purchaseorder['purchase_order_id'];
                                 $purchaseorderinvoice = $this->db->query("SELECT * FROM purchase_order_item WHERE purchase_order_id=$purchaseorderid");
                                 $purchaseorderinvoice1 = $purchaseorderinvoice->num_rows();
                                 ?>
                              <td><?php echo $purchaseorderinvoice1 ?></td>
                              <td><?php echo $purchaseorder['grand_total']; ?></td>
                              <td><?php echo $purchaseorder['status']; ?></td>
                              <td>
                                 <!-- <a class="waves-effect waves-light  btn  submit box-shadow-none border-round mr-1 mb-1" href="<?php echo base_url(); ?>admin/viewpurchaseorder/<?php echo $purchaseorder['purchase_order_id']; ?>" type="submit" name="action">Payment and View
                                    <i class="material-icons left">payment</i>
                                 </a> -->
                                 <a class='dropdown dropdown-trigger waves-effect waves-light  btn edit box-shadow-none border-round mt-2 mb-2 mr-1 mb-1' data-target='dropdown1<?php echo $purchaseorder['purchase_order_id']; ?>'>Click Me</a>

                                 <ul id='dropdown1<?php echo $purchaseorder['purchase_order_id']; ?>' class='dropdown-content'>
                                    <li>
                                       <a class="waves-effect waves-light" href="<?php echo base_url(); ?>admin/viewpurchaseorder/<?php echo $purchaseorder['purchase_order_id']; ?>" style="text-align: center;color: #272361 !important;">Payment and View
                                       </a>
                                       <?php if ($purchaseorder['status']  == 'pending') { ?>
                                          <a class="waves-effect waves-light" href="<?php echo base_url(); ?>admin/update_recived_order/<?php echo $purchaseorder['purchase_order_id']; ?>" style="text-align: center;color: #272361 !important;">Recived
                                          </a>
                                          <a class="waves-effect waves-light" href="<?php echo base_url(); ?>admin/update_cancel_order/<?php echo $purchaseorder['purchase_order_id']; ?>" style="text-align: center;color: #272361 !important;">Cancel
                                          </a>
                                       <?php } elseif ($purchaseorder['status']  == 'recived') { ?>
                                          <a class="waves-effect waves-light" href="<?php echo base_url(); ?>admin/update_pending_order/<?php echo $purchaseorder['purchase_order_id']; ?>" style="text-align: center;color: #272361 !important;">Pending
                                          </a>
                                          <a class="waves-effect waves-light" href="<?php echo base_url(); ?>admin/update_cancel_order/<?php echo $purchaseorder['purchase_order_id']; ?>" style="text-align: center;color: #272361 !important;">Cancel
                                          </a>
                                       <?php } else { ?>
                                          <a class="waves-effect waves-light" href="<?php echo base_url(); ?>admin/update_pending_order/<?php echo $purchaseorder['purchase_order_id']; ?>" style="text-align: center;color: #272361 !important;">Pending
                                          </a>
                                          <a class="waves-effect waves-light" href="<?php echo base_url(); ?>admin/update_recived_order/<?php echo $purchaseorder['purchase_order_id']; ?>" style="text-align: center;color: #272361 !important;">Recived
                                          </a>
                                       <?php }   ?>
                                    </li>
                                 </ul>
                              </td>
                              <td></td>
                           </tr>
                        <?php endforeach; ?>
                        </tfoot>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>