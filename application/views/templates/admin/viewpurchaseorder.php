<style>
   span {
      color: #272361 !important;
   }
</style>
<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card">
            <div class="card-content">
               <h6 class="normalheading">View Purchare info</h6>
               <div class="row">
                  <div class="col s12">
                     <table>
                        <tr>
                           <th>Supplier Name</th>
                           <td><?php echo $purchaseorder['suppliers']; ?></td>
                           <th>Order By</th>
                           <td><?php echo $purchaseorder['order_by']; ?></td>
                        </tr>
                        <tr>
                           <th>Note For Supplier </th>
                           <td><?php echo $purchaseorder['note']; ?></td>
                           <th>Order Date</th>
                           <td><?php echo $purchaseorder['date']; ?></td>
                        </tr>
                        <tr>
                           <th>Status</th>
                           <td><?php echo $purchaseorder['status']; ?></td>
                        </tr>
                     </table>
                  </div>
               </div>
               <h6 class="normalheading">Add Your Payment Here</h6>
               <div class="row">
                  <div class="col s12">
                     <table>
                        <tr>
                           <?php echo form_open('admin/payment'); ?>
                           <th>Add Payment</th>
                           <input type="hidden" name="purchaseorderid" value="<?php echo $purchaseorder['purchase_order_id']; ?>">
                           <td>
                              <span>Your Total Amount</span>
                              <input type="number" value="<?php echo $purchaseorder['grand_total']; ?>" placeholder='0.00' id="order_amount" readonly />
                           </td>
                           <td>
                              <span>Your Remaing Amount</span>
                              <input type="hidden" value="<?php echo $purchaseorder['paid_amount']; ?>" name='paid_amount' id="paid_amount" placeholder='0.00' readonly />
                              <input type="number" id="due_amount" value="0.00" placeholder='0.00' readonly /></td>
                           </td>
                           <td>
                              <span>Type Payment Date</span>
                              <input type="text" class="datepicker" name="paymentdate" placeholder="Type Your Payment Date">
                           </td>
                           <td>
                              <span>Type Your Pay Amount</span>
                              <input type="number" name="payment" onkeyup="onpay(this.value)" value=""  id="pay_amount" placeholder="0.00">
                           </td>
                           <td>
                              <span>Type Your Payment Note</span>
                              <input type="text" name="paymentnote" min="1" max="20" placeholder="Type Your Payment Note">
                           </td>
                           <td>
                              <span>Grand Total Is</span>
                              <input type="number" name='total_amount' id="total_amount" value="0.00" placeholder='0.00' readonly />
                           </td>
                           <td>
                              <button type="submit" class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1">Pay
                              </button></td>
                           <?php echo form_close(); ?>
                        </tr>
                     </table>
                  </div>
               </div>
               <h6 class="normalheading">Mange Purchase Order Item</h6>
               <div class="row">
                  <table id="page-length-option" class="display">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Product</th>
                           <th>Product Image</th>
                           <th>Qty</th>
                           <th>Trade Price</th>
                           <th>Discount</th>
                           <th>Net Amount</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php foreach ($purchaseorderitems as $purchaseorderitem) : ?>
                           <tr>
                              <td><?php echo $purchaseorderitem['product_name_id']; ?></td>
                              <td><?php echo $purchaseorderitem['product']; ?></td>
                              <td><img src="<?php echo base_url(); ?>assets/uploads/<?php if (isset($product['pro_img'])) {
                                                                                          echo $product['pro_img'];
                                                                                       } else {
                                                                                          echo "no-image.png";
                                                                                       }  ?>" width="64px"></td>
                              <td><?php echo $purchaseorderitem['Qty']; ?></td>
                              <td><?php echo $purchaseorderitem['trade_price']; ?></td>
                              <td><?php echo $purchaseorderitem['discount']; ?></td>
                              <td><?php echo $purchaseorderitem['netamount']; ?></td>
                           </tr>
                        <?php endforeach; ?>
                        </tfoot>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
   $(document).ready(function() {
      var orderamount = document.getElementById("order_amount").value;
      var paidamount = document.getElementById("paid_amount").value;
      var totalremainingdue = Number(orderamount) - Number(paidamount);
      document.getElementById("due_amount").value = totalremainingdue;
   });

   function onpay() {
      var payamount = document.getElementById("pay_amount").value;
      var reamain = document.getElementById("due_amount").value
      var total = Number(reamain) - Number(payamount);
      document.getElementById("total_amount").value = total;
   }
</script>
<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>