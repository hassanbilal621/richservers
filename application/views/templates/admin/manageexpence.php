<div id="main">
    <div class="row">
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content">
						<h4 class="card-title">Manage Expence expence</h4>
						<div class="row">
							<table id="page-length-option" class="display">
								<thead>
									<tr>
                                        <th>Expence Id</th>
										<th>expence Catagory</th>                                        
										<th>Expence Price</th>
                                        <th>Expence Note</th>
                                        <th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($expencess as $expence) : ?>
									<tr>
										<td><?php echo $expence['expense_id']; ?></td>
										<td><?php echo $expence['exp_cat']; ?></td>
                                        <td><?php echo $expence['expence_price']; ?></td>
                                        <td><?php echo $expence['expence_note']; ?></td>
                                        <td>
											<button class="waves-effect waves-light  btn edit box-shadow-none border-round mr-1 mb-1 modal-trigger" onclick="loadexpenceinfo(this.id)" id="<?php echo $expence['expense_id']; ?>" type="submit"  href="#modal3"  name="action">EDIT
												<i class="material-icons left">edit</i>
											</button>
                                            <a class="waves-effect waves-light  btn delete box-shadow-none border-round mr-1 mb-1" id="<?php echo $expence['expense_id']; ?>" onclick="del(this.id)" type="submit" name="action">DELETE
                                                <i class="material-icons left">delete_forever</i>
                                            </a>
                                        </td>
									</tr>
									<?php endforeach; ?>
									</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div id="modal3" class="modal">
    <div class="modal-content">
    </div>
</div>
   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
   <script>
      function loadexpenceinfo(expenceid)
      {
         // var userid = this.id;
            $.ajax({
               type: "GET",
               url: "<?php echo base_url();?>admin/ajax_edit_expence_adminmodal/"+expenceid,
               success: function(data){
                  $(".modal-content").html(data);
                  $('#modal3').modal('open');
               }
            });
      }
   </script>
<script>
	function del(id){
	
	        swal({
	            title: "Are you sure?",
	            text: "You will not be able to recover this imaginary file!",
	            icon: 'warning',
	            dangerMode: true,
	            buttons: {
	            cancel: 'No, Please!',
	            delete: 'Yes, Delete It'
	            }
	        }).then(function (willDelete) {
	            if (willDelete) {
	                var html = $.ajax({
	                type: "GET",
	                url: "<?php echo base_url();?>admin/expence_delete/"+id,
	                // data: info,
	                async: false
	                }).responseText;
	
	                if(html == "success")
	                {
	                    $("#delete").html("delete success.");
	                    return true;
	
	                }
	                swal("Poof! Your record has been deleted!", {
	                    icon: "success",
	                });
	                setTimeout(location.reload.bind(location), 1000);
	                } else {
	                swal("Your imaginary file is safe", {
	                    title: 'Cancelled',
	                    icon: "error",
	                });
	                }
	        });  
	
	
	}
	
	
</script>