<!-- BEGIN: SideNav-->
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">

	<!---/////////////////////////////////////////-------------logo-----------///////////////////////////////////////// -->

	<div class="brand-sidebar">
		<h1 class="logo-wrapper">
			<a class="brand-logo darken-1" href="<?php echo base_url(); ?>admin/">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/logo/logo.png" alt="azmi" style="height: 40px;margin: -8px 1px -17px -3px;">
				<span class="logo-text hide-on-med-and-down" style="font-size: 15px;">Azmi Enterprises</span>
			</a>
			<a class="navbar-toggler" href="#">
				<i class="material-icons">radio_button_checked</i>
			</a>
		</h1>
	</div>

	<ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">

		<!---/////////////////////////////////////////-------------Dashboard-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">Dashboard</a>
		</li>
		<li class="bold">
			<a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/dashboard.png" alt="" style="height: 30px;margin: 0px 35px -11px 0;"><span class="menu-title color-02" data-i18n="">Dashboard</span></a>
		</li>

		<!---/////////////////////////////////////////-------------Suppliers-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">Suppliers</a>

		</li>
		<li class="bold">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/supplier.png" alt="" style="height: 40px;margin: 0 20px -13px -5px;">
				<span class="menu-title color-02" data-i18n="">Suppliers</span>
			</a>
			<div class="collapsible-body">
				<ul>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/addsuppliers" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Add Suppliers</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managesuppliers" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage Suppliers</span></a></li>


				</ul>
			</div>
		</li>
		<!---/////////////////////////////////////////-------------Expence Management-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text"> Expence Management</a>
		</li>
		<li class="bold">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/expenses.png" alt="" style="height: 30px;margin: 0px 30px -7px 0px;"><span class="menu-title color-02" data-i18n="">Expence</span></a>
			<div class="collapsible-body">
				<ul class="collapsible collapsible-sub" data-collapsible="accordion">

					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/addexpencecatagoty" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Add Expence Category</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/addexpence" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Add Expence</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/manageexpence" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage Expence</span></a></li>
				</ul>
			</div>
		</li>

		<!---/////////////////////////////////////////-------------Product-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">Product</a>

		</li>
		<li class="bold">
			<a class="collapsible-header waves-effect waves-cyan " href="#"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/product.png" alt="" style="height: 30px;margin: 0 30px -7px 0;"><span class="menu-title color-02" data-i18n="">Product</span></a>
			<div class="collapsible-body">
				<ul>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/addproduct" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Add Product</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/manageproduct" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage Product</span></a></li>
				</ul>
			</div>
		</li>

		<!---/////////////////////////////////////////-------------Purchase Order-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">Purchase Order</a>

		</li>
		<li class="bold">
			<a class="collapsible-header waves-effect waves-cyan " href="#"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/order.png" alt="" style="height: 32px;margin: 0px 20px -7px 7px;"><span class="menu-title color-02" data-i18n="">Purchase Order</span></a>
			<div class="collapsible-body">
				<ul>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/addpurchaseorder" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Make Purchase Order</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managepurchaseorder" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage Purchase Order</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/recivedpurchaseorder" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Recived Purchase Order</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/pandingpurchaseorder" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Pending Purchase Order</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/cancelpurchaseorder" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Cancel Purchase Order</span></a></li>

				</ul>
			</div>
		</li>

		<!---/////////////////////////////////////////-------------Company-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">Company</a>

		</li>
		<li class="bold">
			<a class="collapsible-header waves-effect waves-cyan " href="#"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/company.png" alt="" style="height: 32px;margin: 0px 27px -7px 0px;"><span class="menu-title color-02" data-i18n="">Company</span></a>
			<div class="collapsible-body">
				<ul>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managecompany" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage Company</span></a></li>
					<?php foreach ($companies as $company) : ?>
						<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/company/<?php echo $company['company_id']; ?>" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span><?php echo $company['companyname']; ?></span></a></li>
					<?php endforeach; ?>

				</ul>
			</div>
		</li>

		<!---/////////////////////////////////////////-------------Customer-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">Customer</a>

		</li>
		<li class="bold">
			<a class="collapsible-header waves-effect waves-cyan " href="#"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/customer.png" alt="" style="height: 25px;margin: 0 20px -7px 0px;"><span class="menu-title color-02" data-i18n="">Customer</span></a>
			<div class="collapsible-body">
				<ul>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/addcustomer" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Add Customer</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managecustomer" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage Company</span></a></li>

				</ul>
			</div>
		</li>

		<!---/////////////////////////////////////////-------------Invoice-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">Invoice</a>

		</li>
		<li class="bold">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/invoice.png" alt="" style="height: 31px;margin: 0px 30px -7px 7px;"><span class="menu-title color-02" data-i18n="">Invoice</span></a>
			<div class="collapsible-body">
				<ul>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/addinvoice" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Add Invoice</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/manageinvoice" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Manage Invoice</span></a></li>
				</ul>
			</div>
		</li>
		<!---/////////////////////////////////////////-------------stock-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">Stock</a>
		</li>
		<li class="bold">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/stock.png" alt="" style="height: 30px;margin: 0px 20px -7px 0px;"><span class="menu-title color-02" data-i18n="">stock</span></a>
			<div class="collapsible-body">
				<ul>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/addstock" data-i18n="">
							<i class="material-icons">keyboard_arrow_right</i><span>Add Stock</span></a>
					</li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managestock" data-i18n="">
							<i class="material-icons">keyboard_arrow_right</i><span>Manage Stock</span></a>
					</li>
				</ul>
			</div>
		</li>

		<!---/////////////////////////////////////////-------------Payment-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">Payment</a>
		</li>
		<li class="bold">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/payment.png" alt="" style="height: 30px;margin: 0px 20px -7px 0px;"><span class="menu-title color-02" data-i18n="">Payment</span></a>
			<div class="collapsible-body">
				<ul>
					<!-- <li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/addpayment" data-i18n="">
							<i class="material-icons">keyboard_arrow_right</i><span>Add Payment</span></a>
					</li> -->
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managepayment" data-i18n="">
							<i class="material-icons">keyboard_arrow_right</i><span>Manage Payment</span></a>
					</li>

				</ul>
			</div>
		</li>
		<!---/////////////////////////////////////////-------------Report-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			<a class="navigation-header-text">Report</a>
		</li>
		<li class="bold">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/invoice.png" alt="" style="height: 31px;margin: 0px 30px -7px 0px;"><span class="menu-title color-02" data-i18n="">Reports</span></a>
			<div class="collapsible-body">
				<ul class="collapsible collapsible-sub" data-collapsible="accordion">
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/salereport" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Sale Report</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/purchasereport" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Purchase Report</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/expencereport" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Expence Report</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/salereport" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span>Unpaid Report</span></a></li>
				</ul>
			</div>
		</li>



	</ul>
	<div class="navigation-background"></div>
	<a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->