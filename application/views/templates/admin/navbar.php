   <!-- BEGIN: Header-->
   <style>
     .ml6 {
       position: relative;
       font-weight: 900;
       font-size: 3.3em;
     }

     .ml6 .text-wrapper {
       position: relative;
       display: inline-block;
       /* padding-top: 0.2em;
       padding-right: 0.05em;
       padding-bottom: 0.1em; */
       overflow: hidden;
     }

     .ml6 .letter {
       display: inline-block;
       line-height: 1em;
     }

     p {
       color: black;
     }
   </style>
   <header class="page-topbar" id="header">
     <div class="navbar navbar-fixed">
       <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-light">
         <div class="nav-wrapper">
           <!-- <div class="header-search-wrapper hide-on-med-and-down"><i class="material-icons">search</i>
              <input class="header-search-input z-depth-2" type="text" name="Search" placeholder="Explore Materialize">
            </div> -->
           <ul class="navbar-list right">
             <!-- <li><a class="waves-effect waves-block waves-light notification-button" href="javascript:void(0);" data-target="notifications-dropdown"><i class="material-icons">notifications_none<small class="notification-badge">5</small></i></a></li> -->
             <li><a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online"><img src="<?php echo base_url(); ?>assets/app-assets/images/avatar/avatar-7.png" alt="avatar"><i></i></span></a></li>
           </ul>
          
           <ul class="dropdown-content" id="profile-dropdown">
             <li><a href="<?php echo base_url(); ?>admin/logout" class="grey-text text-darken-1"><i class="material-icons">keyboard_tab</i> Logout</a></li>
           </ul>
         <!-- <center>
              <p><span><span>Beautiful Questions</span> </span></p>
            <p class="ml6">
             <span class="text-wrapper">
               <span class="letters">Beautiful Questions</span>
             </span>
           </p>
           </center>

           <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script> -->
       </nav>
     </div>
   </header>
   <!-- END: Header-->
   <!-- <script>
     // Wrap every letter in a span
     var textWrapper = document.querySelector('.ml6 .letters');
     textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

     anime.timeline({
         loop: true
       })
       .add({
         targets: '.ml6 .letter',
         translateY: ["1.1em", 0],
         translateZ: 0,
         duration: 750,
         delay: (el, i) => 50 * i
       }).add({
         targets: '.ml6',
         opacity: 0,
         duration: 1000,
         easing: "easeOutExpo",
         delay: 1000
       });
   </script> -->