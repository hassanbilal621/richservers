<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card">
            <div class="card-content">
               <h4 class="card-title">Manage Invoices</h4>
               <div class="row">
                  <table id="page-length-option" class="display">
                     <thead>
                        <tr>
                           <th>Invoice id</th>
                           <th>Date</th>
                           <th>Company Name</th>
                           <th>Customer Name</th>
                           <th>Order By</th>
                           <th>Order Note</th>
                           <th>Grand Total</th>
                           <th>View</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php foreach ($invoices as $invoice) : ?>
                           <tr>
                              <td><?php echo $invoice['invoice_id']; ?></td>
                              <td><?php echo $invoice['invoicedate']; ?></td>
                              <td><?php echo $invoice['companyname']; ?></td>
                              <td><?php echo $invoice['customer_name']; ?></td>
                              <td><?php echo $invoice['order_by']; ?></td>
                              <td><?php echo $invoice['order_note']; ?></td>
                              <td><?php echo $invoice['grand_total']; ?></td>
                              <td>
                                 <a class="waves-effect waves-light  btn  submit box-shadow-none border-round mr-1 mb-1" href="<?php echo base_url(); ?>admin/viewinvoice/<?php echo $invoice['invoice_id']; ?>" type="submit" name="action">View Invoice
                                    <i class="material-icons left">visibility</i>
                                 </a>
                                
                              </td>
                              <td></td>
                           </tr>
                        <?php endforeach; ?>
                        </tfoot>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>