<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">
                     <div class="col s10">
                        <h4 class="card-title">Add Product</h4>
                     </div>
                     <div class="col s2 right">
                        <label for="date">Date</label>
                        <input type="date" name="date">
                     </div>
                  </div>
                  <?php echo form_open_multipart('admin/addproduct'); ?>
                  <div>


                     <div class="row">
                        <div class="col s6">
                           <div class="row">
                              <div class="input-field col s12">
                                 <input id="product2" type="text" name="product">
                                 <label for="product2">Product Name</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <select class="form-control" name="product_suppliers_id" required>
                                    <option disabled selected>Select Suppliers</option>
                                    <?php foreach ($suppliers as $supplier) : ?>
                                       <?php if (empty($supplier['suppliers'])) { } else {
                                             ?>
                                          <option value="<?php echo $supplier['suppliers_id']; ?>"><?php echo $supplier['suppliers']; ?></option>
                                       <?php } ?>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <select class="form-control" name="product_cat_id" required>
                                    <option disabled selected>Select Categories</option>
                                    <?php foreach ($categories as $category) : ?>
                                       <?php if (empty($category['cat_name'])) { } else {
                                             ?>
                                          <option value="<?php echo $category['cat_id']; ?>"><?php echo $category['cat_name']; ?></option>
                                       <?php } ?>
                                    <?php endforeach; ?>
                                 </select>
                                 <label>Select Categories</label>
                              </div>
                           </div>
                        </div>
                        <div class="col s6">
                           <div class="row">
                              <div id="file-upload" class="section">
                                 <div class="row section">
                                    <div class="col s12 m12 l12">
                                       <h6 for="img2">Product Image</h6>
                                       <input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <table>
                           <thead>
                              <tr>
                                 <th>Market Price</th>
                                 <th>Trade Price</th>
                                 <th>Pack Size</th>
                                 <th>General Name</th>
                                 <th>Discount Distributer on %</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>
                                    <div class="input-field col s12">
                                       <input id="mktprice" type="number" name="marketprice">
                                       <label for="mktprice">Market Price</label>
                                    </div>
                                 </td>
                                 <td>
                                    <div class="input-field col s12">
                                       <input id="trdprice" type="number" name="tradeprice">
                                       <label for="trdprice">Trade Price</label>
                                    </div>
                                 </td>
                                 <td>
                                    <div class="input-field col s12">
                                       <input id="packsize" type="number" name="packsize">
                                       <label for="packsize">Pack Size</label>
                                    </div>
                                 </td>
                                 <td>
                                    <div class="input-field col s12">
                                       <input id="name" type="text" name="generalname">
                                       <label for="name">General Name</label>
                                    </div>
                                 </td>
                                 <td>
                                    <div class="input-field col s12">
                                       <input id="discount" type="number" name="discount" min="1" max="100">
                                       <label for="discount">Discount Distributer</label>
                                    </div>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                     <div class="row">
                        <div class="input-field col s12">
                           <button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">submit
                              <i class="material-icons right">mode_edit</i>
                           </button>
                        </div>
                     </div>
                     <div class="row">
                        <div class="input-field col s12">
                           <a class="waves-effect waves-light  btn submit-1 box-shadow-none border-round mr-1 mb-1 right" href="<?php echo base_url(); ?>admin/manageproduct">Manage Supllier
                              <i class="material-icons right">send</i></a>
                        </div>
                     </div>

                  </div>
                  <?php echo form_close(); ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>