<div id="main">
	<div class="row">
		<!-- Page Length Options -->
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content">
						<div class="row">
							<div class="col s10">
								<h4 class="card-title">Add Suppliers</h4>
							</div>

							<?php echo form_open_multipart('admin/addsuppliers') ?>
							<div class="row">
								<div class="col s6">
									<div class="row">
										<div class="input-field col s12">
											<input id="name2" type="text" name="suppliers" required>
											<label for="name2">Supplier Name</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<input id="name3" type="email" name="email" required>
											<label for="name3">Email</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<input id="name4" type="text" name="address" required>
											<label for="name4">Address</label>
										</div>
									</div>

								</div>
								<div class="col s6">
									<div id="file-upload" class="section">
										<div class="row section">
											<div class="col s12 m12 l12">
												<h6 for="img2">Supllier Image</h6>
												<input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1" type="submit" name="action">submit
												<i class="material-icons right">mode_edit</i>
											</button>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
											<a class="waves-effect waves-light btn submit-1 z-depth-2 mb-1 ml-1 right" href="<?php echo base_url(); ?>admin/managesuppliers" style="margin: 200px 0 0 0;">Manage Supllier
												<i class="material-icons right">send</i></a>
										</div>
									</div>
								</div>
							</div>
							<?php echo form_close() ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('.datepicker').datepicker();
	});
</script>