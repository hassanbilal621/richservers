<div class="row login-bg">
 	<div class="col s12">
 		<div class="container">
 			<div id="login-page" class="row">
 				<div class="col s8 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
					<?php echo form_open('admin/login'); ?>
					<div class="login-form">
						<div class="row">
							<div class="input-field col s12 center">
							<img src="<?php echo base_url(); ?>/assets/app-assets/images/logo/logo.png" alt="azmi"style="margin: auto;width: 100px;">
			
								<h4>Azmi Enterprises</h4>
								<!-- <img src="<?php echo base_url(); ?>assets/app-assets/images/logo/logo.png" alt="" class="responsive-img valign"> -->
								<?php if ($this->session->flashdata('login_failed')) : ?>
									<div id="card-alert" class="card gradient-45deg-amber-amber">
										<div class="card-content white-text">
											<p> <?php echo $this->session->flashdata('login_failed'); ?></p>
										</div>
										<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
								<?php endif; ?>
								<?php if ($this->session->flashdata('user_loggedout')) : ?>
									<div id="card-alert" class="card green">
										<div class="card-content white-text">
											<p><?php echo $this->session->flashdata('user_loggedout'); ?></p>
										</div>
										<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
									</div>
								<?php endif; ?>
								<p class="center login-form-text">Welcome Back! to admin dashboard</p>
							</div>
						</div>
						<div class="row margin">
 							<div class="input-field col s12">
								 <img src="<?php echo base_url(); ?>assets/app-assets/images/icon/username.png" alt=""  class="prefix" style="padding: 0px 10px 0 0;margin: -3px 0 0 0;">
 								<input id="username" name="username" type="text" placeholder="Username">
 							</div>
 						</div>
 						<div class="row margin">
 							<div class="input-field col s12">
							 <img src="<?php echo base_url(); ?>assets/app-assets/images/icon/pass.png" alt=""  class="prefix" style="padding: 0px 10px 0 0;margin: -3px 0 0 0;">
 								<input id="password" name="password" type="password" placeholder="Password">
 							</div>
 						</div>
						<div class="row">
							<div class="input-field col s12">
								<button type="submit" name="login" class="btn waves-effect #424242 grey darken-3 waves-light col s12">Login</button>
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>
