<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="card">
                        <div class="card-content">
                            <?php echo form_open('admin/addexpence'); ?>
                            <div class="row">
                                <div class="col s10">
                                    <h4 class="card-title">Add Expence</h4>
                                </div>
                                <div class="col s2 right">
                                    <label for="date">Date</label>
                                    <input type="date" class="datepicker" name="date">
                                </div>
                            </div>
                            <div class="input-field col s6">
                                <select name="expense_cat_id">
                                    <option disabled selected value="">Select Expence Catagory </option>
                                    <?php foreach ($categories as $catagory) : ?>
                                        <option value="<?php echo $catagory['exp_cat_id']; ?>"><?php echo $catagory['exp_cat']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="input-field col s6">
                                <input type="number" name="expence_price" placeholder="Add Expence Price" required>
                            </div>
                            <div class="input-field col s12">
                                <input type="text" name="expence_note" placeholder="Add Expence Note" required>
                            </div>
                            <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1" id="view" type="submit" name="action">Submit
                                <i class="material-icons right">send</i>
                            </button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function() {
		$('.datepicker').datepicker();
	});
</script>