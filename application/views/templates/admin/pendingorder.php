<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="card">
				<div class="card-content">
					<h4 class="card-title">Pending Purchaseorder</h4>
					<div class="row">
						<table id="page-length-option" class="display">
							<thead>
								<tr>
									<th>#</th>
									<th>Date</th>
									<th>Supplier Name</th>
									<th>Sub Total</th>
									<th>Discount</th>
									<th>Grand Total</th>
									<th>Order Status</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($purchaseorders as $purchaseorder) : ?>
									<tr>
										<td><?php echo $purchaseorder['purchase_order_id']; ?></td>
										<td><?php echo $purchaseorder['date']; ?></td>
										<td><?php echo $purchaseorder['suppliers']; ?></td>
										<td><?php echo $purchaseorder['sub_total']; ?></td>
										<td><?php echo $purchaseorder['discount']; ?></td>
										<td><?php echo $purchaseorder['grand_total']; ?></td>
										<td><?php echo $purchaseorder['status']; ?></td>
									</tr>
								<?php endforeach; ?>
								</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>