<div id="main">
	<div class="row">
		<!-- Page Length Options -->
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content">
						<h4 class="card-title">Add Categories</h4>
						<div class="row">
							<table id="page-length-option" class="display">
								<thead>
									<tr>
										<th>Company Id</th>
										<th>Company</th>
										<th>Address</th>
                                        <th>Status</th>
                                        <th>Action</th>
									</tr>
								</thead>
								<tbody>
									
                                    <?php foreach($companies as $company): ?>
                                    <tr>
                                        <td><?php echo $company['company_id']; ?></td>
										<td><?php echo $company['companyname']; ?></td>
                                        <td><?php echo $company['companyaddress']; ?></td>
                                        <td><?php echo $company['status']; ?></td>
                                        
										<td>
											<button class="waves-effect waves-light  btn edit box-shadow-none border-round mr-1 mb-1 modal-trigger"  onclick="loadcompanyinfo(this.id)" id="<?php echo $company['company_id']; ?>" type="submit"  href="#modal3"  name="action">EDIT
											<i class="material-icons left">edit</i>
											</button>
                                                <div id="modal2" class="modal">
                                                    <div class="modal-content">
                                                        <div class="row">
                                                            <div class="col s12">
                                                                <div class="card">
                                                                    <?php echo form_open('admin/managecategory') ?>
                                                                    <div class="col s12">
                                                                        <!-- Form with placeholder -->
                                                                        <h4 class="card-title">Edit Categories</h4>
                                                                        <div class="row">
                                                                            <div class="input-field col s12">
                                                                                <input id="name2" type="text" value="<?php echo $category['cat_name']; ?>" name="cat_name">
                                                                                <label for="name2">Add Categories</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="input-field col s12">
                                                                            <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Save
                                                                            <i class="material-icons right">send</i>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php echo form_close() ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <button class="waves-effect waves-light  btn red box-shadow-none border-round mr-1 mb-1 red"  id="<?php echo $company['company_id']; ?>" onclick="del(this.id)" type="submit" name="action">DELETE
											    <i class="material-icons left">delete_forever</i>
                                            </button>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>    
					                
						        </tfoot>
						    </table>
				        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal3" class="modal">
    <div class="modal-content">
    </div>
</div>
   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
   <script>
      function loadcompanyinfo(companyid)
      {
         // var userid = this.id;
            $.ajax({
               type: "GET",
               url: "<?php echo base_url();?>admin/ajax_edit_company_adminmodal/"+companyid,
               success: function(data){
                  $(".modal-content").html(data);
                  $('#modal3').modal('open');
               }
            });
      }
   </script>
<script>
	function del(id){
	
	        swal({
	            title: "Are you sure?",
	            text: "You will not be able to recover this imaginary file!",
	            icon: 'warning',
	            dangerMode: true,
	            buttons: {
	            cancel: 'No, Please!',
	            delete: 'Yes, Delete It'
	            }
	        }).then(function (willDelete) {
	            if (willDelete) {
	                var html = $.ajax({
	                type: "GET",
	                url: "<?php echo base_url();?>admin/deletecompany/"+id,
	                // data: info,
	                async: false
	                }).responseText;
	
	                if(html == "success")
	                {
	                    $("#delete").html("delete success.");
	                    return true;
	
	                }
	                swal("Poof! Your record has been deleted!", {
	                    icon: "success",
	                });
	                setTimeout(location.reload.bind(location), 1000);
	                } else {
	                swal("Your imaginary file is safe", {
	                    title: 'Cancelled',
	                    icon: "error",
	                });
	                }
	        });  
	
	
	}
	
	
</script>